package com.souq.uae.thebooksouq.BroadcastRecivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReciver extends BroadcastReceiver {

    private static SmsListener smsListener;
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        Object[] pdus = (Object[]) data.get("pdus");
        for (int i=0; i<pdus.length;i++)
        {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[])pdus[i]);
            /*String sender = smsMessage.getDisplayOriginatingAddress();
            if (sender.equals("+923064243483"))
            {}*/

            String messageBody = smsMessage.getMessageBody();
            smsListener.messageRecived(messageBody);

        }
    }

    public static void bindListener(SmsListener mListener){smsListener=mListener;}
}
