package com.souq.uae.thebooksouq.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Adapt.SeeAllAdapt;
import com.souq.uae.thebooksouq.Model.SeeAll.Datum;
import com.souq.uae.thebooksouq.Model.SeeAll.SeeAllMain;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SeeAllBooksFragment extends Fragment {

    //private TextView tv_empty;
    private final String TAG = SeeAllBooksFragment.class.getSimpleName();
    private TextView tv_category,tv_noRecord;
    private List<Datum> datumList;
    private RecyclerView mSeeAllBooksRecycler;
    private SeeAllAdapt seeAllAdapt;
    private Context container;
    private SearchView searchView;
    private LinearLayout linearLayout;
    private LinearLayoutManager layoutManager;
    private ProgressBar progressBar, progressBarAfterRecycler;
    private ImageView imageView;
    private String categoryName;
    // protected Handler handler;
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemsCount, previousTotal = 0;
    private int view_Threshold = 4;
    private int pageNo = 1;
    private int check = 0;
    private int mainBookPage = 0;
    private String query;

    public SeeAllBooksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_see_all_books, container, false);
        this.container = container.getContext();
        findViews(view);
        getSeeAllFragmentList();
        mSeeAllBooksRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Picasso picasso = Picasso.get();
                if (mSeeAllBooksRecycler.SCROLL_STATE_IDLE == newState || mSeeAllBooksRecycler.SCROLL_STATE_DRAGGING == newState) {
                    Log.d(TAG, "resumeTag");
                    picasso.resumeTag(container);
                } else {
                    Log.d(TAG, "pauseTag");
                    picasso.pauseTag(container);
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                /*if (dy > 0) {
                    if (check == 0) {

                        if (isLoading) {
                            if (totalItemsCount > previousTotal) {
                                isLoading = false;
                                previousTotal = totalItemsCount;

                            }
                        }
                        if (!isLoading && (totalItemsCount - visibleItemCount) <= (pastVisibleItems + view_Threshold)) {

                            Log.d(TAG, "dy>0:mainBookPage!=0");
                            progressBarAfterRecycler.setVisibility(View.VISIBLE);
                            pageNo++;
                            performPagination();
                            isLoading = true;
                        }

                        }

                        if (check == 1) {
                            if (isLoading) {
                                if (totalItemsCount > previousTotal) {
                                    isLoading = false;
                                    previousTotal = totalItemsCount;

                                }
                            }
                            if (!isLoading && (totalItemsCount - visibleItemCount) <= (pastVisibleItems + view_Threshold)) {
                                progressBarAfterRecycler.setVisibility(View.VISIBLE);
                                pageNo++;
                                searchPagination(query, pageNo);
                                isLoading=true;
                            }
                            }
            }*/
                if (dy > 0) {
                    if (check==0){
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemsCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (isLoading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemsCount) {
                            isLoading = false;
                            pageNo = pageNo + 1;
                            performPagination();
                        }
                    }
                    }


                if (check==1)
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemsCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (isLoading)
                    {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemsCount) {
                            isLoading = false;
                            pageNo=pageNo+1;
                            searchPagination(query, pageNo);
                        }

                    }

                }
            }
            }
        });
        return view;
    }

    private void findViews(View view) {
        tv_noRecord = view.findViewById(R.id.frag_tv_sealboks_no_record);
        categoryName = getArguments().getString(Utils.SEE_ALL_KEY);
       // Toast.makeText(container, categoryName, Toast.LENGTH_SHORT).show();
        linearLayout = view.findViewById(R.id.frag_sealboks_ll);
        mSeeAllBooksRecycler = view.findViewById(R.id.recyclerView_see_all_books);
        layoutManager = (new LinearLayoutManager(container, LinearLayoutManager.VERTICAL, false));
        mSeeAllBooksRecycler.setHasFixedSize(true);
        mSeeAllBooksRecycler.setItemViewCacheSize(2);
        mSeeAllBooksRecycler.setDrawingCacheEnabled(true);
        mSeeAllBooksRecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mSeeAllBooksRecycler.setLayoutManager(layoutManager);
        tv_category = view.findViewById(R.id.fragment_sealbok_categoryName);
        tv_category.setText(categoryName);
        imageView = view.findViewById(R.id.frag_sealboks_imgv);
        searchView = view.findViewById(R.id.frag_sealboks_sv);
        progressBarAfterRecycler = view.findViewById(R.id.frag_sealbok_progressbar_underR);
        progressBar = view.findViewById(R.id.frag_sealbok_progressbar);
        Log.v(TAG, "findViews()");
        searchView.setIconified(false);
        searchView.setQueryHint("Search");
        View v = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        v.setBackgroundColor(Color.TRANSPARENT);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                datumList.clear();
                progressBarAfterRecycler.setVisibility(View.VISIBLE);
                check = 1;
                pageNo = 1;
                query = s;
                firstSearchedQ(query, pageNo);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!(s == null || s.equals("") || s.equals(" ") || s.length() <2)) {
                    //searchSuggest(s);
                    datumList.clear();
                    progressBarAfterRecycler.setVisibility(View.VISIBLE);
                    check = 1;
                    pageNo = 1;
                    query = s;
                     pastVisibleItems=0;
                     visibleItemCount=0;
                     totalItemsCount=0;
                     previousTotal = 0;
                     view_Threshold = 4;
                    firstSearchedQ(query, pageNo);
                } else {
                    //recyclerView.setAdapter(null);
                    datumList.clear();
                    progressBarAfterRecycler.setVisibility(View.VISIBLE);
                    check = 0;
                    pageNo = 1;
                    mainBookPage = 0;
                    pastVisibleItems=0;
                    visibleItemCount=0;
                    totalItemsCount=0;
                    previousTotal = 0;
                    view_Threshold = 4;
                    getSeeAllFragmentList();
                }

                return false;
            }
        });

    }

    private void getSeeAllFragmentList() {

        mSeeAllBooksRecycler.setVisibility(View.GONE);
        tv_noRecord.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        if (Utils.isInternetAvailable(container)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getCategoryItems(categoryName, String.valueOf(pageNo)).enqueue(new Callback<SeeAllMain>() {
                @Override
                public void onResponse(Call<SeeAllMain> call, Response<SeeAllMain> response) {
                    mainBookPage = response.body().getPage();
                    if (response.body() != null) {

                        if (response.body().getBooks() != null) {
                            if (response.body().getBooks().getData() != null) {

                                //datumList.clear();
                                mSeeAllBooksRecycler.setVisibility(View.VISIBLE);
                                tv_noRecord.setVisibility(View.GONE);
                                datumList = response.body().getBooks().getData();
                                seeAllAdapt = new SeeAllAdapt(container, datumList);
                                mSeeAllBooksRecycler.setAdapter(seeAllAdapt);
                                seeAllAdapt.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.VISIBLE);
                                //swipeRefreshLayout.setRefreshing(false);
                                Log.d(TAG, "SeeAllBooksFragmentListUpdating/refreshing");
                            } else {
                                mSeeAllBooksRecycler.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                tv_noRecord.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            mSeeAllBooksRecycler.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            tv_noRecord.setVisibility(View.VISIBLE);
                        }

                    }
                    else {
                        mSeeAllBooksRecycler.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        tv_noRecord.setVisibility(View.VISIBLE);
                    }


                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<SeeAllMain> call, Throwable t) {
                    t.printStackTrace();

                    Log.d(TAG, "SeeAllBooksFragmentListOnFailure");
                }
            });

        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   getSeeAllFragmentList();
                }
            });
            dialog.show();
        }
    }


    private void performPagination() {
        Log.d(TAG, "performing pagination");
        progressBarAfterRecycler.setVisibility(View.VISIBLE);

        if (Utils.isInternetAvailable(container)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getCategoryItems(categoryName, String.valueOf(pageNo)).enqueue(new Callback<SeeAllMain>() {
                @Override
                public void onResponse(Call<SeeAllMain> call, Response<SeeAllMain> response) {
                    Log.d(TAG, response.body().toString());
                    // mainBookPage = response.body().getPage();
                    mainBookPage = response.body().getPage();
                    if (response.body() != null) {
                        if (response.body().getBooks() != null) {
                            if (response.body().getBooks().getData() != null) {

                                List<Datum> datumList = response.body().getBooks().getData();
                                Log.d("TAG", "entered in pagination======" + response.body().getBooks());
                                seeAllAdapt.addData(datumList);
                                isLoading=true;
                            }
                        }
                    }
                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<SeeAllMain> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(TAG, "HomeFragmentListOnFailure");
                }
            });
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    performPagination();
                }
            });
            dialog.show();
        }
    }

    private void firstSearchedQ(final String q,final int pageNo) {
        mSeeAllBooksRecycler.setVisibility(View.GONE);
        tv_noRecord.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        if (Utils.isInternetAvailable(container)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getCategorySearch(categoryName, q, String.valueOf(pageNo)).enqueue(new Callback<SeeAllMain>() {
                @Override
                public void onResponse(Call<SeeAllMain> call, Response<SeeAllMain> response) {
                    mainBookPage = response.body().getPage();
                    if (response.body() != null) {
                        if (response.body().getBooks() != null) {
                            if (response.body().getBooks().getData().size()!=0) {
                                //datumList.clear();
                                mSeeAllBooksRecycler.setVisibility(View.VISIBLE);
                                tv_noRecord.setVisibility(View.GONE);
                                datumList = response.body().getBooks().getData();
                                seeAllAdapt = new SeeAllAdapt(container, datumList);
                                mSeeAllBooksRecycler.setAdapter(seeAllAdapt);
                                seeAllAdapt.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                                linearLayout.setVisibility(View.VISIBLE);
                                isLoading=true;
                                //swipeRefreshLayout.setRefreshing(false);
                                Log.d(TAG, "SeeAllBooksFragmentListUpdating/refreshing");
                            }
                            else {
                                mSeeAllBooksRecycler.setVisibility(View.GONE);
                                progressBar.setVisibility(View.GONE);
                                tv_noRecord.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            mSeeAllBooksRecycler.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            tv_noRecord.setVisibility(View.VISIBLE);
                        }
                    }
                    else {
                        mSeeAllBooksRecycler.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        tv_noRecord.setVisibility(View.VISIBLE);
                    }
                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<SeeAllMain> call, Throwable t) {
                    t.printStackTrace();

                    Log.d(TAG, "SeeAllBooksFragmentListOnFailure");
                }
            });

        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    firstSearchedQ(q,pageNo);
                }
            });
            dialog.show();
        }

    }

    private void searchPagination(final String q,final int pageNo) {
        progressBarAfterRecycler.setVisibility(View.VISIBLE);
        Log.d(TAG, "performing pagination");
        if (Utils.isInternetAvailable(container)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getCategorySearch(categoryName, q, String.valueOf(pageNo)).enqueue(new Callback<SeeAllMain>() {
                @Override
                public void onResponse(Call<SeeAllMain> call, Response<SeeAllMain> response) {
                    Log.d(TAG, response.body().toString());
                    // mainBookPage = response.body().getPage();
                    mainBookPage = response.body().getPage();
                    if (response.body() != null) {
                        if (response.body().getBooks() != null) {
                            if (response.body().getBooks().getData().size()!=0) {
                                List<Datum> datumList = response.body().getBooks().getData();
                                Log.d("TAG", "entered in pagination======" + response.body().getBooks());
                                seeAllAdapt.addData(datumList);
                            }
                        }
                    }
                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<SeeAllMain> call, Throwable t) {
                    t.printStackTrace();
                    Log.d(TAG, "HomeFragmentListOnFailure");
                }
            });
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    searchPagination(q,pageNo);
                }
            });
            dialog.show();
        }
    }
}
