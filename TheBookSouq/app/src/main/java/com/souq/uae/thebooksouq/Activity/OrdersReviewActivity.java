package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Adapt.VerticalReviewAdapt;
import com.souq.uae.thebooksouq.Model.ReviewModel.Review;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersReviewActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private TextView tv_total;
    private RadioGroup radioGroup;
    private RadioButton rb_cod;
    private Button btn_proceed;
    private String checked="0";
    private LinearLayoutManager layoutManager;
    private ProgressBar progressBar;
    private RelativeLayout relativeLayout_orl;
    private VerticalReviewAdapt verticalReviewAdapt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_review);
        findViews();
        performActions();

    }

    @Override
    protected void onResume() {
        relativeLayout_orl.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        orderReview();
        super.onResume();
    }

    private void findViews() {
        toolbar = findViewById(R.id.aor_toolbar);
        recyclerView = findViewById(R.id.aor_recyclerview);
        tv_total = findViewById(R.id.aor_price);
        radioGroup = findViewById(R.id.aor_radiogroup);
        rb_cod = findViewById(R.id.aor_rb_cod);
        btn_proceed= findViewById(R.id.aor_btn_proceed);
        progressBar = findViewById(R.id.aor_progressbar);
        relativeLayout_orl = findViewById(R.id.ora_rl);
        layoutManager = (new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void performActions()
    {

        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(2);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(layoutManager);


        btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (checked.equalsIgnoreCase("1"))*/ {

                    Toast.makeText(OrdersReviewActivity.this, "proceed", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(OrdersReviewActivity.this, CheckOutActivity.class);
                    startActivity(intent);
                }

                }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.aor_rb_cod)
                {
                    checked="1";
                    Toast.makeText(OrdersReviewActivity.this, checked, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void orderReview()
    {
        if (Utils.isInternetAvailable(OrdersReviewActivity.this))
        {
            String token = Utils.getToken(OrdersReviewActivity.this);
            Map<String,String > data = new HashMap<>();
            data.put("user_id",Utils.getUserId(OrdersReviewActivity.this));
            data.put("cart_id",Utils.getCartId(OrdersReviewActivity.this));
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().orderReview(token,data).enqueue(new Callback<Review>() {
                @Override
                public void onResponse(Call<Review> call, Response<Review> response) {
                    if (response.body()!=null)
                    {
                        if (response.body().getStatus().equalsIgnoreCase("ok"))
                        {
                            progressBar.setVisibility(View.GONE);
                            verticalReviewAdapt = new VerticalReviewAdapt(OrdersReviewActivity.this,response.body().getOrderReivew());
                            recyclerView.setAdapter(verticalReviewAdapt);
                            tv_total.setText(String.valueOf(response.body().getTotalPrice().get(0))+" "+"AED");
                            relativeLayout_orl.setVisibility(View.VISIBLE);

                        }

                        else if (response.body().getStatus().equalsIgnoreCase("User not register"))
                        {

                            AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersReviewActivity.this);
                            dialog.setMessage(R.string.login_again);
                            dialog.setCancelable(false);
                            dialog.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Utils.updateUserId(OrdersReviewActivity.this,"-1");
                                    Utils.updateCartId(OrdersReviewActivity.this,"0");
                                    Utils.saveToken(OrdersReviewActivity.this,"0");
                                    Utils.saveCartCount(OrdersReviewActivity.this,0);
                                    Intent intent = new Intent(OrdersReviewActivity.this, HomeActivity.class);
                                    startActivity(intent);

                                }
                            });
                            dialog.show();


                        }
                    }
                }

                @Override
                public void onFailure(Call<Review> call, Throwable t) {
                t.printStackTrace();
                }

            });

        }
        else {


            AlertDialog.Builder dialog = new AlertDialog.Builder(OrdersReviewActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   orderReview();
                }
            });
            dialog.show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();
        switch (itm)
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
