package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.souq.uae.thebooksouq.BroadcastRecivers.SmsListener;
import com.souq.uae.thebooksouq.BroadcastRecivers.SmsReciver;
import com.souq.uae.thebooksouq.Model.Login.Login;
import com.souq.uae.thebooksouq.Model.SignUp.SignUp;
import com.souq.uae.thebooksouq.Model.TwillioVerification.SendSms;
import com.souq.uae.thebooksouq.Model.TwillioVerification.VerifySms;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ValidationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private EditText et_code;
    private Button btn_submit;
    private TextView tv_resendCode;
    private String mobile_no, address, name, email, activityStr;
    private final String TAG = ValidationActivity.class.getSimpleName();
    private AwesomeValidation mAwesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);

        findViews();
        smsReciver();
    }

    private void findViews() {
        toolbar = findViewById(R.id.av_toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        et_code = findViewById(R.id.sRegistration_et_code);
        btn_submit = findViewById(R.id.sRegistration_btn_submit);
        tv_resendCode = findViewById(R.id.sRegistration_tv_resendCode);
        btn_submit.setOnClickListener(this);
        tv_resendCode.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sRegistration_btn_submit:
                mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
                if (et_code.getText().toString().length() < 1) {
                    mAwesomeValidation.addValidation(et_code, RegexTemplate.NOT_EMPTY, getResources().getString(R.string.not_empty));
                }
                mAwesomeValidation.validate();
                if (et_code.getText().toString().length() > 0) {
                    Utils.setProgressDialog(ValidationActivity.this);
                    dataReciving();
                    smsVerification();
                   /* if (activityStr.equalsIgnoreCase("login")) {
                        loginFt(mobile_no);
                    } else {
                        siginingUp();
                    }*/
                }
               break;
            case R.id.sRegistration_tv_resendCode:
                Utils.setProgressDialog(this);
                twillioSendSms(mobile_no);
                break;
        }
    }

    private void smsReciver() {
        SmsReciver.bindListener(new SmsListener() {
            @Override
            public void messageRecived(String messageText) {
                if (messageText.contains("112-112")) {
                    et_code.setText(messageText);
                }
            }
        });
    }

    private void twillioSendSms(final String phoneNo) {
        Map<String, String> data = new HashMap<>();
        data.put("via", "sms");
        data.put("phone_number", phoneNo);
        data.put("country_code", Utils.COUNTRY_CODE);
        if (Utils.isInternetAvailable(ValidationActivity.this)) {

            LoginRetrofitClass.getTwilioInstance().getTwillioService().sendSms(Utils.TWILLIO_API_KEY, data).enqueue(new Callback<SendSms>() {
                @Override
                public void onResponse(Call<SendSms> call, Response<SendSms> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(ValidationActivity.this, response.body().getMessage());

                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(ValidationActivity.this, response.body().getMessage());
                        }
                    } else {
                        Utils.disappearProgressDialog();
                        Utils.alertDialog(ValidationActivity.this, getString(R.string.invalid_no));
                    }
                }

                @Override
                public void onFailure(Call<SendSms> call, Throwable t) {
                    Utils.disappearProgressDialog();
                    t.printStackTrace();
                }
            });
        } else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(ValidationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    twillioSendSms(phoneNo);
                }
            });
            dialog.show();
        }
    }

    private void smsVerification() {
        Map<String, String> data = new HashMap<>();
        data.put("phone_number", mobile_no);
        data.put("country_code", Utils.COUNTRY_CODE);
        data.put("verification_code", et_code.getText().toString());

        if (Utils.isInternetAvailable(ValidationActivity.this)) {
            LoginRetrofitClass.getTwilioInstance().getTwillioService().verifySms(Utils.TWILLIO_API_KEY, data).enqueue(new Callback<VerifySms>() {
                @Override
                public void onResponse(Call<VerifySms> call, Response<VerifySms> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            if (activityStr.equalsIgnoreCase("login")) {
                                loginFt(mobile_no);
                            } else {
                                siginingUp();
                            }

                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(ValidationActivity.this, response.body().getMessage());

                        }
                    } else {
                        Utils.disappearProgressDialog();
                        Utils.alertDialog(ValidationActivity.this, getString(R.string.invalid));

                    }

                }

                @Override
                public void onFailure(Call<VerifySms> call, Throwable t) {
                    t.printStackTrace();
                    Utils.disappearProgressDialog();
                }
            });
        }else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(ValidationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    smsVerification();
                }
            });
            dialog.show();
        }
    }


    private void siginingUp() {

        Map<String, String> data = new HashMap<>();
        data.put("full_name", name);
        data.put("email", email);
        data.put("mobile", mobile_no);
        data.put("address", address);
        if (!Utils.getUserId(ValidationActivity.this).equalsIgnoreCase("-1")) {
            data.put("user_id", Utils.getUserId(ValidationActivity.this));
        }

        if (Utils.isInternetAvailable(this)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().signUp(data).enqueue(new Callback<SignUp>() {
                @Override
                public void onResponse(Call<SignUp> call, Response<SignUp> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 200) {
                            Utils.disappearProgressDialog();
                            Utils.saveToken(ValidationActivity.this, response.body().getToken());
                            Utils.updateUserId(ValidationActivity.this, String.valueOf(response.body().getUserID()));
                            Intent intent = new Intent(ValidationActivity.this, RegisteredActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(ValidationActivity.this, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<SignUp> call, Throwable t) {
                    t.printStackTrace();
                    Utils.disappearProgressDialog();
                    Utils.alertDialog(ValidationActivity.this, "Connectivity problem.");
                }
            });
        }
        else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(ValidationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   siginingUp();
                }
            });
            dialog.show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();
        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void loginFt(final String phoneNo) {
        if (Utils.isInternetAvailable(ValidationActivity.this)) {
            Map<String, String> data = new HashMap<>();
            data.put("mobile", phoneNo);
            if (!(Utils.getUserId(ValidationActivity.this).equalsIgnoreCase("-1"))) {
                data.put("user_id", Utils.getUserId(ValidationActivity.this));
            }
            if (!(Utils.getCartId(ValidationActivity.this).equalsIgnoreCase("-1") || Utils.getCartId(ValidationActivity.this).equalsIgnoreCase("0"))) {
                data.put("cart_id", Utils.getCartId(ValidationActivity.this));
            }
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().login(data).enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("ok")) {
                            String token = response.body().getToken();
                            String userId = String.valueOf(response.body().getUserId());
                            String cartId = String.valueOf(response.body().getCartId());
                            Utils.saveToken(ValidationActivity.this, token);
                            Utils.updateUserId(ValidationActivity.this, userId);
                            Utils.updateCartId(ValidationActivity.this, cartId);
                            Utils.disappearProgressDialog();
                            Intent intent = new Intent(ValidationActivity.this, RegisteredActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(ValidationActivity.this, response.body().getStatus());
                        }
                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    t.printStackTrace();
                    Utils.disappearProgressDialog();
                }
            });
        }
        else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(ValidationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                  loginFt(phoneNo);
                }
            });
            dialog.show();
        }
    }

    private void dataReciving() {

        name = getIntent().getStringExtra("full_name");
        address = getIntent().getStringExtra("address");
        email = getIntent().getStringExtra("email");
        mobile_no = getIntent().getStringExtra("mobile");
        activityStr = getIntent().getStringExtra("activity");
    }
}
