package com.souq.uae.thebooksouq.Model.HereGeocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("Relevance")
    @Expose
    private Integer relevance;
    @SerializedName("Distance")
    @Expose
    private Double distance;
    @SerializedName("MatchLevel")
    @Expose
    private String matchLevel;
    @SerializedName("MatchQuality")
    @Expose
    private MatchQuality matchQuality;
    @SerializedName("MatchType")
    @Expose
    private String matchType;
    @SerializedName("Location")
    @Expose
    private Location location;

    public Integer getRelevance() {
        return relevance;
    }

    public void setRelevance(Integer relevance) {
        this.relevance = relevance;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getMatchLevel() {
        return matchLevel;
    }

    public void setMatchLevel(String matchLevel) {
        this.matchLevel = matchLevel;
    }

    public MatchQuality getMatchQuality() {
        return matchQuality;
    }

    public void setMatchQuality(MatchQuality matchQuality) {
        this.matchQuality = matchQuality;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}