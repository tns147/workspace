package com.souq.uae.thebooksouq.Model.MyOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetail_ {

    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("expected_delivery_date")
    @Expose
    private String expectedDeliveryDate;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("pending_approvel")
    @Expose
    private Boolean pendingApprovel;
    @SerializedName("approved")
    @Expose
    private Boolean approved;
    @SerializedName("out_for_delivery")
    @Expose
    private Boolean outForDelivery;
    @SerializedName("delivered")
    @Expose
    private Boolean delivered;
    @SerializedName("book_image")
    @Expose
    private List<String> bookImage = null;

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Boolean getPendingApprovel() {
        return pendingApprovel;
    }

    public void setPendingApprovel(Boolean pendingApprovel) {
        this.pendingApprovel = pendingApprovel;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Boolean getOutForDelivery() {
        return outForDelivery;
    }

    public void setOutForDelivery(Boolean outForDelivery) {
        this.outForDelivery = outForDelivery;
    }

    public Boolean getDelivered() {
        return delivered;
    }

    public void setDelivered(Boolean delivered) {
        this.delivered = delivered;
    }

    public List<String> getBookImage() {
        return bookImage;
    }

    public void setBookImage(List<String> bookImage) {
        this.bookImage = bookImage;
    }

}