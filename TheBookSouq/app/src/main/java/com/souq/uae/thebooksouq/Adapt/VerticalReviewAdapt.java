package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Model.ReviewModel.OrderReivew;
import com.souq.uae.thebooksouq.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

public class VerticalReviewAdapt extends RecyclerView.Adapter{

    private List<OrderReivew>orderReivewList;
    private Context context;



    public VerticalReviewAdapt(Context context, List<OrderReivew>orderReivewList)
    {
        this.orderReivewList = orderReivewList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vertical_review_layout,viewGroup,false);
        viewHolder = new VerticalReviewAdapterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof VerticalReviewAdapterViewHolder)
        {
            OrderReivew orderReivew = orderReivewList.get(i);
            VerticalReviewAdapterViewHolder vh=((VerticalReviewAdapterViewHolder)holder);
                vh.tv_merchant.setText(orderReivew.getMerchant());
                vh.tv_subTotal.setText(String.valueOf(orderReivew.getPriceByMerchant())+" "+"AED");
                vh.tv_bookQuantity.setText(String.valueOf(orderReivew.getMerchantBooksQuantity()));
                vh.tv_deliveryCharges.setText("0"+" "+"AED");
                ViewPagerAdapterOrderReview viewPagerAdapterOrderReview = new ViewPagerAdapterOrderReview(context,orderReivew.getData());
                vh.viewPager.setAdapter(viewPagerAdapterOrderReview);
                vh.circlePageIndicator.setViewPager(vh.viewPager);
                final float density = context.getResources().getDisplayMetrics().density;
                vh.circlePageIndicator.setRadius(3 * density);
        }
    }

    @Override
    public int getItemCount() {
        return orderReivewList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class VerticalReviewAdapterViewHolder extends RecyclerView.ViewHolder{
        //private RecyclerView recyclerView;
        private ViewPager viewPager;
//        private ViewPagerAdapterOrderReview viewPagerAdapterOrderReview;
        private TextView tv_bookQuantity,tv_subTotal,tv_merchant,tv_deliveryCharges;
        private CirclePageIndicator circlePageIndicator;

        public VerticalReviewAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
        tv_bookQuantity = itemView.findViewById(R.id.vrl_tv_noOfBooks);
        tv_deliveryCharges = itemView.findViewById(R.id.vrl_tv_deliveryCharges);
        tv_subTotal = itemView.findViewById(R.id.vrl_tv_subTotal);
        tv_merchant = itemView.findViewById(R.id.vrl_tv_merchnat);
        //recyclerView = itemView.findViewById(R.id.vrl_recylcer);
        viewPager = itemView.findViewById(R.id.vrl_view_pager);
        circlePageIndicator = itemView.findViewById(R.id.vrl_indicator);
        }
    }
}
