package com.souq.uae.thebooksouq.Model.HereGeocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MapReference {

    @SerializedName("ReferenceId")
    @Expose
    private String referenceId;
    @SerializedName("MapId")
    @Expose
    private String mapId;
    @SerializedName("MapVersion")
    @Expose
    private String mapVersion;
    @SerializedName("MapReleaseDate")
    @Expose
    private String mapReleaseDate;
    @SerializedName("Spot")
    @Expose
    private Double spot;
    @SerializedName("SideOfStreet")
    @Expose
    private String sideOfStreet;
    @SerializedName("CountryId")
    @Expose
    private String countryId;
    @SerializedName("StateId")
    @Expose
    private String stateId;
    @SerializedName("CountyId")
    @Expose
    private String countyId;
    @SerializedName("CityId")
    @Expose
    private String cityId;
    @SerializedName("BuildingId")
    @Expose
    private String buildingId;
    @SerializedName("AddressId")
    @Expose
    private String addressId;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getMapVersion() {
        return mapVersion;
    }

    public void setMapVersion(String mapVersion) {
        this.mapVersion = mapVersion;
    }

    public String getMapReleaseDate() {
        return mapReleaseDate;
    }

    public void setMapReleaseDate(String mapReleaseDate) {
        this.mapReleaseDate = mapReleaseDate;
    }

    public Double getSpot() {
        return spot;
    }

    public void setSpot(Double spot) {
        this.spot = spot;
    }

    public String getSideOfStreet() {
        return sideOfStreet;
    }

    public void setSideOfStreet(String sideOfStreet) {
        this.sideOfStreet = sideOfStreet;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(String buildingId) {
        this.buildingId = buildingId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

}
