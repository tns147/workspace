package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.souq.uae.thebooksouq.Model.TwillioVerification.SendSms;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileNoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText et_mobile;
    private Button btn_next;
    private AwesomeValidation mAwesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_no);
        findViews();
        performActions();

    }

    private void findViews() {
        toolbar = findViewById(R.id.amn_toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        et_mobile = findViewById(R.id.amn_et_mobileno);
        btn_next = findViewById(R.id.amn_btn_next);
    }

    private void performActions() {
        mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNo = et_mobile.getText().toString();
                if (phoneNo.length() != 9) {
                    mAwesomeValidation.addValidation(et_mobile, "[0-9]{9}", getResources().getString(R.string.phoneno_validity_msg));
                }
                mAwesomeValidation.validate();
                if (phoneNo.length() == 9) {
                    //Utils.setProgressDialog(MobileNoActivity.this);
                    //twillioSendSms(phoneNo);

                    Intent intent = new Intent(MobileNoActivity.this, ValidationActivity.class);
                    intent.putExtra("mobile",phoneNo);
                    startActivity(intent);

                }
            }


        });
    }

    private void twillioSendSms(final String phoneNo) {
        if (Utils.isInternetAvailable(MobileNoActivity.this)) {
            Map<String, String> data = new HashMap<>();
            data.put("via", "sms");
            data.put("phone_number", phoneNo);
            data.put("country_code", Utils.COUNTRY_CODE);

            LoginRetrofitClass.getTwilioInstance().getTwillioService().sendSms(Utils.TWILLIO_API_KEY, data).enqueue(new Callback<SendSms>() {
                @Override
                public void onResponse(Call<SendSms> call, Response<SendSms> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            Utils.disappearProgressDialog();
                            Intent intent = new Intent(MobileNoActivity.this, ValidationActivity.class);
                            intent.putExtra("mobile",phoneNo);
                            startActivity(intent);
                        }

                        else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(MobileNoActivity.this, response.body().getMessage());
                        }
                    }
                    else {
                        Utils.disappearProgressDialog();
                        Utils.alertDialog(MobileNoActivity.this,getString(R.string.invalid_no));
                    }
                }

                @Override
                public void onFailure(Call<SendSms> call, Throwable t) {
                    Utils.disappearProgressDialog();
                    t.printStackTrace();
                }
            });
        }         else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(MobileNoActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    twillioSendSms(phoneNo);
                }
            });
            dialog.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm= item.getItemId();
        switch (itm)
        {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}