package com.souq.uae.thebooksouq.Model.SearchResults;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResults {

    @SerializedName("books")
    @Expose
    private Book books;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Book getBooks() {
        return books;
    }

    public void setBooks(Book books) {
        this.books = books;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}