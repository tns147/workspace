package com.souq.uae.thebooksouq.Model.ReverseGeoModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchQuality {

    @SerializedName("Country")
    @Expose
    private Integer country;
    @SerializedName("State")
    @Expose
    private Integer state;
    @SerializedName("County")
    @Expose
    private Integer county;
    @SerializedName("City")
    @Expose
    private Integer city;
    @SerializedName("District")
    @Expose
    private Integer district;
    @SerializedName("Street")
    @Expose
    private List<Integer> street = null;
    @SerializedName("HouseNumber")
    @Expose
    private Integer houseNumber;
    @SerializedName("PostalCode")
    @Expose
    private Integer postalCode;

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCounty() {
        return county;
    }

    public void setCounty(Integer county) {
        this.county = county;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public List<Integer> getStreet() {
        return street;
    }

    public void setStreet(List<Integer> street) {
        this.street = street;
    }

    public Integer getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Integer houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

}