package com.souq.uae.thebooksouq.Model;

public class AllBooksModel {
    private String price;
    private String bookName;
    private String authorName;
    private String merchantName;
    private String imgCover;


    public AllBooksModel(String price,String bookName,String authorName,String merchantName,String imgCover)
    {
     this.authorName = authorName;
     this.bookName =bookName;
     this.price = price;
     this.merchantName = merchantName;
     this.imgCover = imgCover;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getImgCover() {
        return imgCover;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setImgCover(String imgCover) {
        this.imgCover = imgCover;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

}
