package com.souq.uae.thebooksouq.Model.BookDescription;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookDescription {

    @SerializedName("book_details")
    @Expose
    private List<BookDetail> bookDetails = null;
    @SerializedName("more_like_this")
    @Expose
    private MoreLikeThis moreLikeThis;

    public List<BookDetail> getBookDetails() {
        return bookDetails;
    }

    public void setBookDetails(List<BookDetail> bookDetails) {
        this.bookDetails = bookDetails;
    }

    public MoreLikeThis getMoreLikeThis() {
        return moreLikeThis;
    }

    public void setMoreLikeThis(MoreLikeThis moreLikeThis) {
        this.moreLikeThis = moreLikeThis;
    }

}