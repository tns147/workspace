package com.souq.uae.thebooksouq.Model;

public class HorizonModel {
    private String itemName;
    private String itemImage;

    public HorizonModel(String itemImage,String itemName)
    {
        this.itemImage = itemImage;
        this.itemName = itemName;
    }

    public String getItemImage() {
        return itemImage;
    }

    public String getItemName() {
        return itemName;
    }
}
