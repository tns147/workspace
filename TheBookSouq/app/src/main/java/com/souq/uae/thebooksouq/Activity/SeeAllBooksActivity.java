package com.souq.uae.thebooksouq.Activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.souq.uae.thebooksouq.Fragments.SeeAllBooksFragment;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

public class SeeAllBooksActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView textCartItemCount;
    private String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_books);

        findViews();
        if (savedInstanceState == null) {
            SeeAllBooksFragment seeAllBooksFragment= new SeeAllBooksFragment();

            Bundle args = new Bundle();
            args.putString(Utils.SEE_ALL_KEY,data);
            seeAllBooksFragment.setArguments(args);
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_activity_see_all_books, seeAllBooksFragment);
           // fragmentTransaction.addToBackStack(null);

            fragmentTransaction.commit();
        }
    }

    private void findViews()
    {
        data = getIntent().getStringExtra(Utils.SEE_ALL_KEY);
        toolbar = findViewById(R.id.toolbar_activity_see_all_books);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        final MenuItem menuItem = menu.findItem(R.id.menu_cart);
        if (!Utils.getToken(SeeAllBooksActivity.this).equalsIgnoreCase("0"))
        {
            MenuItem menu_notification = menu.findItem(R.id.menu_notification);
            MenuItem menu_profile = menu.findItem(R.id.menu_signin);
            menu_notification.setVisible(true);
            menu_profile.setVisible(false);
           // invalidateOptionsMenu();
        }
        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.cart_badge);
        setupBadge(Utils.getCartCount(this));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            case R.id.menu_cart:
                Intent intent = new Intent(this,CartActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_signin:
              //  Toast.makeText(this, "signIn", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this,RegistrationActivity.class);
                startActivity(intent1);
                break;
            case R.id.menu_notification:
              //  Toast.makeText(this, "bell", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_home:
                Intent intent2 = new Intent(this,HomeActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setupBadge(int cartItems) {

        if (textCartItemCount != null) {
            if (cartItems == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(cartItems, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        super.onResume();
    }
}


