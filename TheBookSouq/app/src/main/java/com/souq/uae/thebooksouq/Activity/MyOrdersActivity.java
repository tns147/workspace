package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;



import com.souq.uae.thebooksouq.Adapt.VerticalMyordersAdapt;
import com.souq.uae.thebooksouq.Fragments.SearchFragment;
import com.souq.uae.thebooksouq.Model.MyOrders.MyOrders;
import com.souq.uae.thebooksouq.Model.MyOrdersModel;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView textCartItemCount;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
   // private List<MyOrdersModel> myOrdersModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);
       // prepareData();
        findViews();

        //        inListViews();
    /*    if (savedInstanceState == null) {
            MyOrdersFragment myOrdersFragment= new MyOrdersFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_activity_my_orders, myOrdersFragment);
            fragmentTransaction.commit();
        }*/
    }

    private void findViews()
    {
        progressBar = findViewById(R.id.progressbar_myorders);
        toolbar = findViewById(R.id.toolbar_activity_myOrders);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.amo_recycler);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        final MenuItem menuItem = menu.findItem(R.id.menu_cart);
        if (!Utils.getToken(MyOrdersActivity.this).equalsIgnoreCase("0"))
        {
            MenuItem menu_notification = menu.findItem(R.id.menu_notification);
            MenuItem menu_profile = menu.findItem(R.id.menu_signin);
            menu_notification.setVisible(true);
            menu_profile.setVisible(false);
           // invalidateOptionsMenu();
        }

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.cart_badge);
        setupBadge(Utils.getCartCount(this));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            case R.id.menu_cart:
                Intent intent = new Intent(this,CartActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_signin:
               // Toast.makeText(this, "signIn", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this,RegistrationActivity.class);
                startActivity(intent1);
                break;
            case R.id.menu_notification:
                //Toast.makeText(this, "bell", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_home:
                Intent intent2 = new Intent(this,HomeActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    public void setupBadge(int cartItems) {

        if (textCartItemCount != null) {
            if (cartItems == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(cartItems, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        getMyOrders();
        invalidateOptionsMenu();
        super.onResume();
    }

/*
    private void inListViews()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        VerticalMyordersAdapt verticalMyordersAdapt = new VerticalMyordersAdapt(this);
        recyclerView.setAdapter(verticalMyordersAdapt);
    }
*/

    private void getMyOrders()
    {
        if (Utils.isInternetAvailable(this))
        {
            String userId =Utils.getUserId(MyOrdersActivity.this);
            if (userId.equalsIgnoreCase("-1"))
            {
                userId="0";
            }
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().myOrders(userId).enqueue(new Callback<MyOrders>() {
                @Override
                public void onResponse(Call<MyOrders> call, Response<MyOrders> response) {

                    if (response.body()!=null)
                    {
                        if (response.body().getStatus().equalsIgnoreCase("ok"))
                        {
                            recyclerView.setLayoutManager(new LinearLayoutManager(MyOrdersActivity.this,LinearLayoutManager.VERTICAL,false));
                            VerticalMyordersAdapt verticalMyordersAdapt = new VerticalMyordersAdapt(MyOrdersActivity.this,response.body());
                            recyclerView.setAdapter(verticalMyordersAdapt);
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<MyOrders> call, Throwable t)
                {
                    progressBar.setVisibility(View.GONE);
                    t.printStackTrace();
                }
            });
        }
        else {
            progressBar.setVisibility(View.GONE);
            AlertDialog.Builder dialog = new AlertDialog.Builder(MyOrdersActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                     getMyOrders();
                }
            });
            dialog.show();
        }
    }
}
