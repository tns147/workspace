package com.souq.uae.thebooksouq.Model.ReviewModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Review {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("order_reivew")
    @Expose
    private List<OrderReivew> orderReivew = null;
    @SerializedName("total_price")
    @Expose
    private List<Double> totalPrice = null;

    @SerializedName("delivery_charges")
    @Expose
    private String delivery_charges;


    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderReivew> getOrderReivew() {
        return orderReivew;
    }

    public void setOrderReivew(List<OrderReivew> orderReivew) {
        this.orderReivew = orderReivew;
    }

    public List<Double> getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(List<Double> totalPrice) {
        this.totalPrice = totalPrice;
    }

}