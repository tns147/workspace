package com.souq.uae.thebooksouq.Model.HereGeocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {

    @SerializedName("MetaInfo")
    @Expose
    private MetaInfo metaInfo;
    @SerializedName("View")
    @Expose
    private List<View> view = null;

    public MetaInfo getMetaInfo() {
        return metaInfo;
    }

    public void setMetaInfo(MetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    public List<View> getView() {
        return view;
    }

    public void setView(List<View> view) {
        this.view = view;
    }

}
