package com.souq.uae.thebooksouq.Model.HomeFragmentModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("merchant")
    @Expose
    private String merchant;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("discount_price")
    @Expose
    private Double discount_price;

    @SerializedName("discount_percentage")
    @Expose
    private Double discount_percentage;

    @SerializedName("discount_available")
    @Expose
    private boolean discount_available;

    public Double getDiscount_percentage() {
        return discount_percentage;
    }

    public boolean isDiscount_available() {
        return discount_available;
    }

    public Double getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_percentage(Double discount_percentage) {
        this.discount_percentage = discount_percentage;
    }

    public void setDiscount_price(Double discount_price) {
        this.discount_price = discount_price;
    }

    public void setDiscount_available(boolean discount_available) {
        this.discount_available = discount_available;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

}