package com.souq.uae.thebooksouq.Model.DeleteCartItem;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteItem {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("total_price")
    @Expose
    private Double totalPrice;
    @SerializedName("Msg")
    @Expose
    private String msg;
    @SerializedName("cart_quantity")
    @Expose
    private String cart_quantity;

    public void setCart_quantity(String cart_quantity) {
        this.cart_quantity = cart_quantity;
    }

    public String getCart_quantity() {
        return cart_quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}