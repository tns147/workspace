package com.souq.uae.thebooksouq.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LongDef;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Activity.SearchActivity;
import com.souq.uae.thebooksouq.Activity.SearchrResultsActivity;
import com.souq.uae.thebooksouq.Adapt.SearchResultsAdapter;
import com.souq.uae.thebooksouq.Model.SearchResults.Datum;
import com.souq.uae.thebooksouq.Model.SearchResults.SearchResults;

import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchResultsFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private TextView tv_browseResults, tv_no_record;
    private List<Datum> datumList;
    private SearchResultsAdapter searchResultsAdapter;
    private RelativeLayout relativeLayout;
    private ProgressBar progressBar, progressBarAfterRecycler;
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemsCount, previousTotal = 0;
    private int view_Threshold = 4;
    private int pageNo = 1;
    private int mainBookPage = 0;
    private String searchedQuery;
    private Button btn_filter, btn_sort;
    private AlertDialog alertDialogCB, alertDialogRB;
    private String st_author, st_gener, st_book, st_merchant;
    private EditText et_search;
    private Map<String, String> data = new HashMap<>();
    private int check = 0;
    private int sort = 0;
    private String TAG = SearchResultsFragment.class.getSimpleName();

    private CheckBox cb_author, cb_gener, cb_book, cb_merchant;
    private RadioGroup radioGroup;
    //private RadioButton rb_pop,rb_price_asc,rb_price_dsc,r;

    public SearchResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_results, container, false);
        this.context = container.getContext();
        Log.d(TAG, "onCreateView");

        findViews(view);
        data.put("q", searchedQuery);
        data.put("page", String.valueOf(1));
        getSearchedResults(data);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
               /* visibleItemCount = layoutManager.getChildCount();
                totalItemsCount = layoutManager.getItemCount();
                pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {

                    Toast.makeText(context, "check 1", Toast.LENGTH_SHORT).show();
                    if (isLoading) {
                        if (totalItemsCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemsCount;
                        }
                    }
                    if (!isLoading && (totalItemsCount - visibleItemCount) <= (pastVisibleItems + view_Threshold)) {
                        pageNo = pageNo + 1;

                        data.put("q", searchedQuery);
                        data.put("page", String.valueOf(pageNo));
                        performPagination(data);
                         isLoading = true;
                    }
                }*/
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemsCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (isLoading)
                    {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemsCount) {
                            isLoading = false;
                            pageNo=pageNo+1;
                            data.put("q", searchedQuery);
                            data.put("page", String.valueOf(pageNo));
                            performPagination(data);
                        }

                    }
                }

            }
        });
        return view;
    }

    private void findViews(View view) {
        // radioGroup = view.findViewById(R.id.radioGroup);

        //radioButton = view.findViewById(R.id.)
        //     alertDialogCB = new AlertDialog.Builder(context).create();
        tv_no_record = view.findViewById(R.id.frag_tv_no_results_found);
        alertDialogRB = new AlertDialog.Builder(context).create(); //Read Update
        //Read Update
        searchedQuery = getArguments().getString(Utils.SEARCH_SUBMIT_KEY);
       // Toast.makeText(context, searchedQuery, Toast.LENGTH_SHORT).show();
        recyclerView = view.findViewById(R.id.frag_search_results_recycler);
        layoutManager = (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(5);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(layoutManager);
        relativeLayout = view.findViewById(R.id.frag_search_rll);
        progressBarAfterRecycler = view.findViewById(R.id.frag_search_results_progressBar_underR);
        progressBar = view.findViewById(R.id.frag_search_results_progressBar);
        btn_filter = view.findViewById(R.id.frag_search_results_filter);
        btn_sort = view.findViewById(R.id.frag_search_results_sort);
        btn_filter.setOnClickListener(this);
        btn_sort.setOnClickListener(this);

    }

    private void performPagination(final Map<String, String> data) {
        progressBarAfterRecycler.setVisibility(View.VISIBLE);
        if (Utils.isInternetAvailable(context)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getBrowseSearchResults(data).enqueue(new Callback<SearchResults>() {
                @Override
                public void onResponse(Call<SearchResults> call, Response<SearchResults> response) {
                    mainBookPage = response.body().getPage();
                    //Toast.makeText(context, String.valueOf(mainBookPage), Toast.LENGTH_SHORT).show();

                    if (response.body() != null) {
                        // mainBookPage = response.body().getPage();
                        if (response.body().getBooks() != null) {
                            if (response.body().getBooks().getData().size() > 0) {

                                searchResultsAdapter.addDataInSearchResults(response.body().getBooks().getData());
                                isLoading = true;
                            }
                        }
                    }
                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<SearchResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
        else {
            android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    performPagination(data);
                }
            });
            dialog.show();
        }
    }


    private void getSearchedResults(final Map<String, String> data) {
        recyclerView.setVisibility(View.GONE);
        tv_no_record.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        if (Utils.isInternetAvailable(context)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getBrowseSearchResults(data).enqueue(new Callback<SearchResults>() {
                @Override
                public void onResponse(Call<SearchResults> call, Response<SearchResults> response) {
                    mainBookPage = response.body().getPage();

                    if (response.body() != null) {
                        //tv_no_record.setVisibility(View.GONE);

                        if (response.body().getBooks() != null) {
                          //  tv_no_record.setVisibility(View.GONE);
                            if (response.body().getBooks().getData().size() > 0) {
                                tv_no_record.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                datumList = response.body().getBooks().getData();
                                searchResultsAdapter = new SearchResultsAdapter(context, response.body().getBooks().getData());
                                recyclerView.setAdapter(searchResultsAdapter);
                                searchResultsAdapter.notifyDataSetChanged();
                                progressBar.setVisibility(View.GONE);
                                relativeLayout.setVisibility(View.VISIBLE);
                            } else {
                                progressBar.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                tv_no_record.setVisibility(View.VISIBLE);
                            }

                        } else {
                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                            tv_no_record.setVisibility(View.VISIBLE);
                        }

                    } else {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        tv_no_record.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<SearchResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
        else {
            android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getSearchedResults(data);
                 }
            });
            dialog.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frag_search_results_filter:

                alertDialogCB = new AlertDialog.Builder(context).create();
                Activity activity = (Activity) context;
                LayoutInflater adbInflater = activity.getLayoutInflater();
                View checkboxLayout = adbInflater.inflate(R.layout.check_boxes, null);
                cb_author = checkboxLayout.findViewById(R.id.cbl_search_checkbox_author);
                cb_book = checkboxLayout.findViewById(R.id.cbl_search_checkbox_bookTitle);
                cb_merchant = checkboxLayout.findViewById(R.id.cbl_search_checkbox_merchant);
                cb_gener = checkboxLayout.findViewById(R.id.cbl_search_checkbox_genre);
                et_search = checkboxLayout.findViewById(R.id.cbl_et_layout);
                et_search.setText(searchedQuery);
                Log.i("et_search_before", et_search.getText().toString());
                alertDialogCB.setView(checkboxLayout);
                alertDialogCB.setTitle("Filter By");

                alertDialogCB.setButton(Dialog.BUTTON_POSITIVE, "Save changes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub


                        data = new HashMap<>();
                        /*if (sort>0) {
                            data.put("sort", String.valueOf(sort));
                        }*/
                        if (cb_author.isChecked()) {
                       //     Toast.makeText(getActivity(), "cb Author checked", Toast.LENGTH_LONG).show();
                            st_author = "true";
                            data.put("author", st_author);
                            Log.d(TAG, "author put");

                        } else {
                     //       Toast.makeText(getActivity(), "cb Author Unchecked", Toast.LENGTH_LONG).show();
                            st_author = "false";
                            Log.d(TAG, "author removed");
                        }


                        if (cb_book.isChecked()) {
                          //  Toast.makeText(getActivity(), "cb book checked", Toast.LENGTH_LONG).show();
                            st_book = "true";
                            data.put("title", st_book);
                            Log.d(TAG, "title put");
                        } else {
                         //   Toast.makeText(getActivity(), "cb book Unchecked", Toast.LENGTH_LONG).show();
                            st_book = "false";
                            Log.d(TAG, "title removed");
                        }


                        if (cb_gener.isChecked()) {
                          //  Toast.makeText(getActivity(), "cb gener checked", Toast.LENGTH_LONG).show();
                            st_gener = "true";
                            data.put("category", st_gener);
                            Log.d(TAG, "category put");
                        } else {
                           // Toast.makeText(getActivity(), "cb gener Unchecked", Toast.LENGTH_LONG).show();
                            st_gener = "false";

                        }


                        if (cb_merchant.isChecked()) {
                          //  Toast.makeText(getActivity(), "cb merchant", Toast.LENGTH_LONG).show();
                            st_merchant = "true";
                            data.put("merchant", st_merchant);
                            Log.d(TAG, "merchant put");
                        } else {
                         //   Toast.makeText(getActivity(), "cb merchant Unchecked", Toast.LENGTH_LONG).show();
                            st_merchant = "false";
                            //data.remove("merchant");
                        }

                        if (st_merchant.equals("true") || st_author.equals("true") || st_gener.equals("true") || st_book.equals("true") || (!et_search.getText().toString().isEmpty())) {
                            isLoading = true;
                            pastVisibleItems = 0;
                            visibleItemCount = 0;
                            totalItemsCount = 0;
                            previousTotal = 0;
                            view_Threshold = 4;

                            pageNo = 1;
                            check = 1;
                            Log.i("et_search_after", et_search.getText().toString());
                            searchedQuery = et_search.getText().toString();
                            Log.i("searchedQuery", et_search.getText().toString());
                            data.put("q", searchedQuery);
                            data.put("page", String.valueOf(pageNo));
                            Log.d("if main condition one", data.toString());
                            getSearchedResults(data);

                        } else {
                           // Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                            isLoading = true;
                            pastVisibleItems = 0;
                            visibleItemCount = 0;
                            totalItemsCount = 0;
                            previousTotal = 0;
                            view_Threshold = 4;
                            pageNo = 1;
                            check = 0;
                            data = new HashMap<>();
                            searchedQuery = et_search.getText().toString();
                            data.put("q", searchedQuery);
                            data.put("page", String.valueOf(pageNo));
                            Log.d("else condition main", data.toString());
                            getSearchedResults(data);
                        }


                    }
                });
                alertDialogCB.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialogCB.dismiss();
                    }
                });
                alertDialogCB.show();
                break;

            case R.id.frag_search_results_sort:
                //alertDialog = new AlertDialog.Builder(context).create(); //Read Update

                Activity activity1 = (Activity) context;
                LayoutInflater adbInflater1 = activity1.getLayoutInflater();
                View radioBtnLayout = adbInflater1.inflate(R.layout.radio_buttons_sortby, null);
                radioGroup = radioBtnLayout.findViewById(R.id.radioGroup);

                alertDialogRB.setView(radioGroup);
                alertDialogRB.setTitle("Sort by");
                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        if (i == R.id.rb_sortby_bestsellers) {
                            sort = 7;
                        } else if (i == R.id.rb_sortby_newbooks) {
                            sort = 1;
                        } else if (i == R.id.rb_sortby_oldbooks) {
                            sort = 2;
                        } else if (i == R.id.rb_sortby_popularity) {

                        } else if (i == R.id.rb_sortby_priceasc) {
                            sort = 4;
                        } else if (i == R.id.rb_sortby_pricedsc) {
                            sort = 3;
                        } else if (i == R.id.rb_sortby_titleasc) {
                            sort = 5;
                        } else if (i == R.id.rb_sortby_titledsc) {
                            sort = 6;
                        }
                    }
                });
                alertDialogRB.setButton(Dialog.BUTTON_POSITIVE, "Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (sort > 0 & sort < 8) {
                            isLoading = true;
                            pastVisibleItems = 0;
                            visibleItemCount = 0;
                            totalItemsCount = 0;
                            previousTotal = 0;
                            view_Threshold = 4;
                            pageNo = 1;
                            check = 1;
                            String val = String.valueOf(sort);
                            data.put("sort", val);
                            data.put("page", String.valueOf(pageNo));
                            getSearchedResults(data);
                        }
                        alertDialogRB.dismiss();
                    }
                });
                alertDialogRB.setButton(Dialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sort = 5;
                        alertDialogRB.dismiss();
                    }
                });
                alertDialogRB.show();


                break;

        }
    }
}
