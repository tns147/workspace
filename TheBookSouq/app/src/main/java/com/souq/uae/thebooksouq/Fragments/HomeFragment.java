package com.souq.uae.thebooksouq.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Activity.SearchActivity;

import com.souq.uae.thebooksouq.Adapt.VerticalAdapt;
import com.souq.uae.thebooksouq.Activity.HomeActivity;
import com.souq.uae.thebooksouq.Listeners.OnLoadMoreListener;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.Book;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.MainBooks;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements /*GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,*/ View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private TextView tvEmptyView, tvSearch, tvLocation, tvChangeLocation;

    private RecyclerView mVerticalList;
    private VerticalAdapt verticalAdapt;
   // private DataAdapter dataAdapter;
    private RelativeLayout relativeLayout;
    private ProgressBar progressBar, progressBarAfterRecycler;
    private LinearLayoutManager layoutManager;


    private Context container;
    //private GoogleApiClient mLocationClient;
    // private LocationRequest mLocationRequest = new LocationRequest();
    private String TAG = "HomeFragment";
    private SwipeRefreshLayout swipeRefreshLayout;
    //protected Handler handler;
    //private String longitude, latitude;
    private ImageView imageView;
    private List<Book> bookList = new ArrayList<>();
    // private int starting=1;
    // private int itemCount=5;
    private boolean isLoading = true;
    private int pastVisibleItems=0, visibleItemCount=0, totalItemsCount=0, previousTotal = 0;
    private int view_Threshold = 1;
    int pageNo = 1;
    //int mainBookPage = 0;


    public HomeFragment() {


    }

    @Override
    public void onResume() {
        isLoading = true;
        pastVisibleItems = 0;
        visibleItemCount = 0;
        totalItemsCount = 0;
        previousTotal = 0;
        view_Threshold = 1;
        pageNo = 1;
       // mainBookPage = 0;
        getHomeFragmentList();
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_home, container, false);
        this.container = container.getContext();


        findViews(view);

        // tvLocation.setText(Utils.getLocation(this.container));
        getHomeFragmentList();
        mVerticalList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.v("HomeFragment", "onScrolled()");

                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemsCount = layoutManager.getItemCount();
                    pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    if (isLoading)
                    {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemsCount) {
                            isLoading = false;
                            pageNo=pageNo+1;
                            performPagination(pageNo);
                        }

                        }
                    }


                    /*if (isLoading) {
                        if (totalItemsCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totalItemsCount;
                        }
                    }
                    if (!isLoading && (totalItemsCount - visibleItemCount) <= (pastVisibleItems + view_Threshold)) {
                        pageNo=pageNo+1;
                        performPagination(pageNo);
                        isLoading = true;
                    }
                }*/
            }
        });
        mVerticalList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Picasso picasso = Picasso.get();
                if (recyclerView.SCROLL_STATE_IDLE == newState || recyclerView.SCROLL_STATE_DRAGGING == newState) {
                    Log.d(TAG, "resumeTag");
                    picasso.resumeTag(container);
                } else {
                    Log.d(TAG, "pauseTag");
                    picasso.pauseTag(container);
                }
            }
        });

        return view;


    }


    private void findViews(View view) {

        Log.v("HomeFragment", "findViews()");
        // tvLocation = view.findViewById(R.id.tv_fHome_location);
        //tvChangeLocation = view.findViewById(R.id.tv_fHome_changeLocation);


        imageView = view.findViewById(R.id.img_homeFrag);
        imageView.setOnClickListener(this);
        tvSearch = view.findViewById(R.id.textView_homeFrag);
        tvSearch.setOnClickListener(this);
        tvEmptyView = view.findViewById(R.id.empty_view);
        mVerticalList = view.findViewById(R.id.vertical_list);

        //handler = new Handler();
        relativeLayout = view.findViewById(R.id.fragment_home_relativeLayout);

        layoutManager = (new LinearLayoutManager(container, LinearLayoutManager.VERTICAL, false));
        mVerticalList.setHasFixedSize(true);
        mVerticalList.setItemViewCacheSize(4);
     //   mVerticalList.setDrawingCacheEnabled(true);
       // mVerticalList.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
      //  mVerticalList.setNestedScrollingEnabled(false);
        mVerticalList.setLayoutManager(layoutManager);


        progressBar = view.findViewById(R.id.fragment_home_progressBar);
        progressBarAfterRecycler = view.findViewById(R.id.fragment_home_uRecycler_progressBar);

        swipeRefreshLayout = view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);



    }


    private void getHomeFragmentList() {


        if (Utils.isInternetAvailable(container)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getHomedata(String.valueOf(pageNo)).enqueue(new Callback<MainBooks>() {
                @Override
                public void onResponse(Call<MainBooks> call, Response<MainBooks> response) {
                    if (response.body() != null) {
                       // mainBookPage = response.body().getPage();
                        bookList = response.body().getBooks();
                      //  dataAdapter = new DataAdapter(container,bookList,mVerticalList);
                        verticalAdapt = new VerticalAdapt(container, bookList, mVerticalList);
                        mVerticalList.setAdapter(verticalAdapt);
                       // dataAdapter.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                        relativeLayout.setVisibility(View.VISIBLE);
                        swipeRefreshLayout.setRefreshing(false);
                        Log.d("HomeFragment", "HomeFragmentListUpdating/refreshing");
                        if (bookList.isEmpty()) {
                            mVerticalList.setVisibility(View.GONE);
                            tvEmptyView.setVisibility(View.VISIBLE);
                        } else {
                            mVerticalList.setVisibility(View.VISIBLE);
                            tvEmptyView.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<MainBooks> call, Throwable t) {
                    t.printStackTrace();

                    Log.d("HomeFragment", "HomeFragmentListOnFailure");
                }
            });

        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getHomeFragmentList();
                }
            });
            dialog.show();
        }
    }


    @Override
    public void onRefresh() {
        isLoading = true;
        pastVisibleItems = 0;
        visibleItemCount = 0;
        totalItemsCount = 0;
        previousTotal = 0;
        view_Threshold = 1;
        pageNo = 1;
        //mainBookPage = 0;
        getHomeFragmentList();


    }

    private void performPagination(final int pageNo) {
        Log.d("HomeFragment", "performing pagination");


        if (Utils.isInternetAvailable(container)) {

            progressBarAfterRecycler.setVisibility(View.VISIBLE);
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getHomedata(String.valueOf(pageNo)).enqueue(new Callback<MainBooks>() {
                @Override
                public void onResponse(Call<MainBooks> call, Response<MainBooks> response) {
                    Log.d("this is responce body ", response.body().toString());

                   // mainBookPage = response.body().getPage();
                    if (response.body().getBooks().size() > 0) {

                        List<Book> books = response.body().getBooks();
                        Log.d(TAG, "entered in pagination======" + response.body().getBooks());
                        bookList.addAll(books);
//                        verticalAdapt.addBooks(books);
                    verticalAdapt.notifyDataSetChanged();
                    isLoading=true;
                    }
                    progressBarAfterRecycler.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<MainBooks> call, Throwable t) {
                    t.printStackTrace();

                    Log.d("HomeFragment", "homepaginationfailure");
                }
            });
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(container);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    performPagination(pageNo);
                }
            });
            dialog.show();
        }
    }

    @Override
    public void onClick(View view) {
        SearchFragment searchFragment = new SearchFragment();
        HomeActivity homeActivity = (HomeActivity) container;
        FragmentTransaction fragmentTransaction = homeActivity.getSupportFragmentManager().beginTransaction();
        switch (view.getId()) {
            case R.id.img_homeFrag:

                Intent intent = new Intent(container, SearchActivity.class);
                startActivity(intent);

                break;
            case R.id.textView_homeFrag:
                Intent intenti = new Intent(container, SearchActivity.class);
                startActivity(intenti);
                break;
        }
    }
}
