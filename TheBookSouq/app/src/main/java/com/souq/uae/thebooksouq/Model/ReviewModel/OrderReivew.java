package com.souq.uae.thebooksouq.Model.ReviewModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderReivew {

    @SerializedName("merchant")
    @Expose
    private String merchant;
    @SerializedName("price_by_merchant")
    @Expose
    private Double priceByMerchant;
    @SerializedName("merchant_books_quantity")
    @Expose
    private Integer merchantBooksQuantity;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public Double getPriceByMerchant() {
        return priceByMerchant;
    }

    public void setPriceByMerchant(Double priceByMerchant) {
        this.priceByMerchant = priceByMerchant;
    }

    public Integer getMerchantBooksQuantity() {
        return merchantBooksQuantity;
    }

    public void setMerchantBooksQuantity(Integer merchantBooksQuantity) {
        this.merchantBooksQuantity = merchantBooksQuantity;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}