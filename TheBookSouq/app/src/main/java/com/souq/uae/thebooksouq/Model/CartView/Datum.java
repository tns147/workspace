package com.souq.uae.thebooksouq.Model.CartView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("book_id")
    @Expose
    private Integer bookId;
    @SerializedName("book_price")
    @Expose
    private Double bookPrice;
    @SerializedName("book_name")
    @Expose
    private String bookName;
    @SerializedName("book_image")
    @Expose
    private String bookImage;
    @SerializedName("book_quantity")
    @Expose
    private Integer bookQuantity;
    @SerializedName("merchant_name")
    @Expose
    private String merchantName;
    @SerializedName("quantity_price")
    @Expose
    private Double quantityPrice;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Double getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(Double bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public Integer getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(Integer bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public Double getQuantityPrice() {
        return quantityPrice;
    }

    public void setQuantityPrice(Double totalPrice) {
        this.quantityPrice = totalPrice;
    }

}