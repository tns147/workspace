package com.souq.uae.thebooksouq.Listeners;

public interface OnLoadMoreListener {
    void onLoadMore();
}
