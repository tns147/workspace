package com.souq.uae.thebooksouq.Model.CartView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartQuantity {

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
