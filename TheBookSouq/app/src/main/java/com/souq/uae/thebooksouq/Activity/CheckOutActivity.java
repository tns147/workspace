package com.souq.uae.thebooksouq.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.souq.uae.thebooksouq.Model.CheckOut.CheckOut;
import com.souq.uae.thebooksouq.Model.Login.Login;
import com.souq.uae.thebooksouq.Model.OrderComplete.OrderComplete;
import com.souq.uae.thebooksouq.Model.OrderProceed.OrderProcced;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textCartItemCount;
    private Toolbar toolbar;
    private TextView tv_change, tv_address, tv_date, tv_payment;
    private Button btn_complete;
    private RadioGroup radioGroup;
    private RadioButton rb_cod, rb_cc;
    private EditText et_address;
    private ProgressBar progressBar;

    private AlertDialog alertDialog;
    private String response_rb = "0";
    private MaterialSpinner materialSpinner;
    private LayoutInflater layoutInflater;
    private List<String> locations;
    private static final String TAG = CheckOutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        findViews();
        performActions();
    }

    private void findViews() {
        toolbar = findViewById(R.id.checkout_toolbar);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tv_change = findViewById(R.id.checkout_tv_change);
        tv_address = findViewById(R.id.checkout_tv_address);
        tv_date = findViewById(R.id.checkout_tv_date);
        btn_complete = findViewById(R.id.checkout_btn_complete);
        radioGroup = findViewById(R.id.checkout_rg);
        rb_cc = findViewById(R.id.checkout_rb_cc);
        rb_cod = findViewById(R.id.checkout_rb_cod);
        tv_payment = findViewById(R.id.checkout_tv_payment);
        progressBar = findViewById(R.id.ac_progressbar);
    }

    private void performActions() {
        layoutInflater = this.getLayoutInflater();
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(100); //You can manage the time of the blink with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        tv_change.startAnimation(anim);
        tv_change.setOnClickListener(this);
        btn_complete.setOnClickListener(this);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                btn_complete.setBackgroundResource(R.drawable.button_selector);
                if (i == R.id.checkout_rb_cc) {
                    response_rb = "cc";
                }

                if (i == R.id.checkout_rb_cod) {
                    response_rb = "cod";
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.checkout_tv_change:
                tv_change.clearAnimation();
                alertDialog = new AlertDialog.Builder(CheckOutActivity.this).create();
                View edittextLayout = layoutInflater.inflate(R.layout.edittext_layout, null);
                et_address = edittextLayout.findViewById(R.id.etl_edittext);
                et_address.setText(tv_address.getText().toString());
                alertDialog.setView(edittextLayout);
                alertDialog.setTitle(getResources().getString(R.string.deliver_address));

                /*
                materialSpinner = spinnerLayout.findViewById(R.id.sl_spinner);
                locations.add("UAE");
                locations.add("Pak");
                locations.add("Get Current Location");
                materialSpinner.setItems(locations);
*/
                alertDialog.setView(edittextLayout);
                alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        tv_address.setText(et_address.getText().toString());
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();

                break;
            case R.id.checkout_btn_complete:
                if (!response_rb.equalsIgnoreCase("0")) {

                    if (response_rb.equalsIgnoreCase("cc")) {
                     //   orderComplete(response_rb);
                        Toast.makeText(this, response_rb, Toast.LENGTH_SHORT).show();
                    }

                    if (response_rb.equalsIgnoreCase("cod")) {
                        Utils.setProgressDialog(CheckOutActivity.this);
                        orderComplete(response_rb);
                    }
                } else {
                    Utils.alertDialog(CheckOutActivity.this, getResources().getString(R.string.payment_method_select));
                }
                Log.d(TAG, "userID:" + Utils.getUserId(CheckOutActivity.this));
                Log.d(TAG, "cartID:" + Utils.getCartId(CheckOutActivity.this));
                Log.d(TAG, "Token:" + Utils.getToken(CheckOutActivity.this));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_book_description, menu);
        final MenuItem menuItem = menu.findItem(R.id.menu_cart);
        if (!Utils.getToken(CheckOutActivity.this).equalsIgnoreCase("0")) {
            MenuItem menu_notification = menu.findItem(R.id.menu_notification);
            MenuItem menu_profile = menu.findItem(R.id.menu_signin);
            menu_notification.setVisible(true);
            menu_profile.setVisible(false);
            //invalidateOptionsMenu();
        }

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.cart_badge);
        setupBadge(Utils.getCartCount(this));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            case R.id.menu_cart:
                Intent intent = new Intent(this, CartActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_signin:
                Toast.makeText(this, "signIn", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this, RegistrationActivity.class);
                startActivity(intent1);
                break;
            case R.id.menu_notification:
                Toast.makeText(this, "bell", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_home:
                Intent intent2 = new Intent(this,HomeActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupBadge(int cartItems) {

        if (textCartItemCount != null) {
            if (cartItems == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(cartItems, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        btn_complete.setBackgroundResource(R.drawable.button_selector_quantity);
        orderProceed();
        super.onResume();
    }

    private void orderProceed() {
        if (Utils.isInternetAvailable(CheckOutActivity.this)) {
            String token = Utils.getToken(CheckOutActivity.this);
            Map<String, String> data = new HashMap<>();
            data.put("user_id", Utils.getUserId(CheckOutActivity.this));
            data.put("cart_id", Utils.getCartId(CheckOutActivity.this));

            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().orderProceed(token, data).enqueue(new Callback<OrderProcced>() {
                @Override
                public void onResponse(Call<OrderProcced> call, Response<OrderProcced> response) {
                    if (response.body().getStatus().equalsIgnoreCase("ok")) {
//                        tv_address.setText(response.body().get);
                        progressBar.setVisibility(View.GONE);
                        tv_date.setText(response.body().getExpectedDeliveryDate());
                        tv_payment.setText(String.valueOf(response.body().getTotalPrice()) + " " + "AED");
                        if (response.body().isCash_payment_allowed()) {
                            rb_cod.setVisibility(View.VISIBLE);
                        }
                    } else if (response.body().getStatus().equalsIgnoreCase("User not register")) {
                        Utils.alertDialog(CheckOutActivity.this, response.body().getStatus());
                        Utils.saveToken(CheckOutActivity.this, "0");
                        progressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(CheckOutActivity.this, RegistrationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }

                @Override
                public void onFailure(Call<OrderProcced> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
        else {
            android.app.AlertDialog.Builder dialog = new android.app.AlertDialog.Builder(CheckOutActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    orderProceed();
                }
            });
            dialog.show();
        }
    }

    private void orderComplete(final String paymentType) {
        if (Utils.isInternetAvailable(CheckOutActivity.this)) {
            String token = Utils.getToken(CheckOutActivity.this);
            Map<String, String> data = new HashMap<>();
            data.put("user_id", Utils.getUserId(CheckOutActivity.this));
            data.put("cart_id", Utils.getCartId(CheckOutActivity.this));
            data.put("payment_type", paymentType);
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().orderComplete(token, data).enqueue(new Callback<OrderComplete>() {
                @Override
                public void onResponse(Call<OrderComplete> call, Response<OrderComplete> response) {
                    if (response.body().getStatus().equalsIgnoreCase("ok")) {
                        Toast.makeText(CheckOutActivity.this, "Complete", Toast.LENGTH_SHORT).show();
                        Utils.saveCartCount(CheckOutActivity.this, 0);
                        Utils.updateCartId(CheckOutActivity.this, "0");
                        Utils.setOrderId(CheckOutActivity.this, String.valueOf(response.body().getOrderId()));
                        invalidateOptionsMenu();
                        Utils.disappearProgressDialog();
                        alertDialog = new AlertDialog.Builder(CheckOutActivity.this).create();
                        View orderstatus_layout = layoutInflater.inflate(R.layout.order_status_layout, null);
                        //et_address = edittextLayout.findViewById(R.id.etl_edittext);
                        //et_address.setText(tv_address.getText().toString());
                        alertDialog.setView(orderstatus_layout);
                        alertDialog.setView(orderstatus_layout);
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                alertDialog.dismiss();
                                Intent intent = new Intent(CheckOutActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);

                            }
                        });
                        alertDialog.show();
                    }
                    else {
                        Utils.disappearProgressDialog();
                    }

                }

                @Override
                public void onFailure(Call<OrderComplete> call, Throwable t) {
                    t.printStackTrace();
                    Utils.disappearProgressDialog();
                }
            });

        }
        else {
            Utils.disappearProgressDialog();
            AlertDialog.Builder dialog = new AlertDialog.Builder(CheckOutActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    orderComplete(paymentType);
                }
            });
            dialog.show();
        }
    }
}
