package com.souq.uae.thebooksouq.Service;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.souq.uae.thebooksouq.Activity.MyOrdersActivity;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private String TAG =MyFirebaseMessagingService.class.getSimpleName();
    private String title,message;
    private Random random = new Random();
    @Override
    public void onNewToken(String s) {
        Utils.setFirebaseToken(this,s);
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
            String messageBody = remoteMessage.getNotification().getBody();
        Intent intent = new Intent("com.souq.uae.thebooksouq_TARGET_NOTIFICATION");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.consumer_logo_round)
                        .setContentTitle("The Book Souq")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Firebase Channel",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());



/*        Log.d(TAG,remoteMessage.getFrom());

        if (remoteMessage.getData().size()==0)
        {
            Log.d(TAG,"Message data payload is null");
        }
        if (remoteMessage.getData().size()>0)
        {
            String data = remoteMessage.getData().get("data");
            if (data==null)
            {
                return;
            }
            try {
                JSONObject dataObject = new JSONObject(data);
                JSONObject dataInnerObject = dataObject.getJSONObject("data");
                title = dataInnerObject.getString("title");
                message = dataInnerObject.getString("message");
            }
            catch (JSONException e){
                e.printStackTrace();
            }*/
         /*   Log.d(TAG,"Message data payload"+remoteMessage.getData());
            Intent intent = new Intent(getApplicationContext(),MyOrdersActivity.class);
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);



            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "The Book Souq Consumer")
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.consumer_logo))
                    .setSmallIcon(R.drawable.consumer_logo_round)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            *//*if (MyPrefrences.getInstance(getApplicationContext()).getCheck(MyPrefrences.ISNOTIFON))*//*
                notificationManager.notify(random.nextInt(1), notificationBuilder.build());

*/


  //      }
        if (remoteMessage.getNotification()!=null)
        {
            Log.d(TAG,"Message Notification Body:"+remoteMessage.getNotification().getBody());

            //sendNotification(remoteMessage.getNotification().getBody());
        }

    }
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MyOrdersActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.consumer_logo_round)
                        .setContentTitle("The Book Souq")
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Firebase Channel",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
