package com.souq.uae.thebooksouq.Model.MyOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetail {

    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("order_details")
    @Expose
    private List<OrderDetail_> orderDetails = null;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public List<OrderDetail_> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail_> orderDetails) {
        this.orderDetails = orderDetails;
    }

}