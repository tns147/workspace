package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.souq.uae.thebooksouq.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    //private AlphaTransformation alphaTransformation;
    private Context context;
    private List<String> imageUrlList;
    public ViewPagerAdapter(Context context, List<String>imageUrlList)
    {
        this.context = context;
        this.imageUrlList=imageUrlList;
       // this.alphaTransformation = alphaTransformation;
    }

    @Override
    public int getCount() {
        return imageUrlList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(context);
       // imageView.requestLayout();
       // imageView.getLayoutParams().height = 200;
       // imageView.getLayoutParams().width = 150;
        imageView.setPadding(0,150,0,50);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setAdjustViewBounds(true);
        imageView.setBackgroundResource(R.drawable.imageview_dropshaddow_bg);
//        imageView.setBackgroundResource(R.drawable.drop_shadow);

        Picasso.get().load(imageUrlList.get(position)).placeholder(R.drawable.image_placeholder).into(imageView);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
       container.removeView((View)object);
    }
}
