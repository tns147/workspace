package com.souq.uae.thebooksouq.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Utils {

    public static final String COUNTRY_CODE = "971";
    public static final String TWILLIO_API_KEY ="jEyt3xX34yROe1tqavayal1TjcFbHLLi";
    public static final String TWILLIO_URL ="https://api.authy.com/";
    private static final String prefOrderId = "prefsorderid";
    private static final String orderId = "orderId";
    private static final String prefsTokenId = "prefstoken";
    private static final String tokenId = "tokenId";
    private static final String prefsCartId = "prefscartid";
    private static final String cartId = "cartid";
    private static final String prefsUserId = "prefsuserid";
    private static final String userId = "userid";
    private static final String prefsCart = "cart";
    private static final String cartCount = "0";
    private static final String prefsLocation = "location";
    private static final String address = "home";
    private static AlertDialog dialog ;
    public static final String GEOCODE_BASE_URL = "https://maps.googleapis.com/maps/";
    //samaid//192.168.2.24:8080
    //faizan//http://192.168.2.22:8080
    public static final String HOME_BASE_URL_IMAGE = "http://23.94.46.103:8000";
    public static final String HOME_BASE_URL = "http://23.94.46.103:8000/";
    public static final String HERE_BASE_URL = "https://reverse.geocoder.api.here.com/";
    public static final String HERE_APP_ID = "1OcNSrooUu1S3rT6Eid6";
    public static final String HERE_APP_CODE = "kHb4SktDdGvyd-L3eNbXsg";
    public static final String HERE_RADIUS="250";

    public static final String LOCATION_IQ_URL = "https://us1.locationiq.com/";
    public static final String LOCATION_IQ_KEY = "c2f1e96857187a";

    public static final String API_KEY_GEOCODE = "AIzaSyBoZcC_G5CLB_V7Cpki9Zx9if_uP71mRDQ";
    public static final String SEE_ALL_KEY = "sa";
    public static final String SEARCH_SUBMIT_KEY = "ss";
    public static final String BOOK_ID = "bid";
    public static final String PREFS_FIREBASE_TOKEN = "fbtoken";
    public static final String FIREBASE_TOKEN="fbtkn";



    public static void setFirebaseToken(Context context,String fbToken)
    {
        SharedPreferences sp = context.getSharedPreferences(PREFS_FIREBASE_TOKEN,Context.MODE_PRIVATE);
        sp.edit().putString(FIREBASE_TOKEN,fbToken).apply();
    }

    public String getFirebaseToken(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences(PREFS_FIREBASE_TOKEN, Context.MODE_PRIVATE);

        return sp.getString(FIREBASE_TOKEN, "0");
    }


    public static void setOrderId(Context context, String order_Id)
    {
        SharedPreferences sp = context.getSharedPreferences(prefOrderId,Context.MODE_PRIVATE);
        sp.edit().putString(orderId,order_Id).apply();
    }

    public String getOrderId(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences(prefOrderId, Context.MODE_PRIVATE);

        return sp.getString(orderId, "0");
    }

    public static void saveToken(Context context,String token)
    {
        SharedPreferences sp = context.getSharedPreferences(prefsTokenId,Context.MODE_PRIVATE);
        sp.edit().putString(tokenId,token).apply();
    }

    public static String getToken(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences(prefsTokenId, Context.MODE_PRIVATE);

        return sp.getString(tokenId, "0");
    }

    public static void saveCartCount(Context context, int no) {

        SharedPreferences sp = context.getSharedPreferences(prefsCart, Context.MODE_PRIVATE);

        sp.edit().putInt(cartCount, no).apply();

    }

    public static int getCartCount(Context context) {
        SharedPreferences sp = context.getSharedPreferences(prefsCart, Context.MODE_PRIVATE);

        return sp.getInt(cartCount, 0);

    }

    public static void updateLocation(Context context, String add) {

        SharedPreferences sp = context.getSharedPreferences(prefsLocation, Context.MODE_PRIVATE);

        sp.edit().putString(address, add).apply();

    }

    public static String getLocation(Context context) {
        SharedPreferences sp = context.getSharedPreferences(prefsLocation, Context.MODE_PRIVATE);
        return sp.getString(address, "UAE");
    }

    public static void updateUserId(Context context, String id) {
        SharedPreferences sp = context.getSharedPreferences(prefsUserId, Context.MODE_PRIVATE);
        sp.edit().putString(userId, id).apply();

    }

    public static String getUserId(Context context) {
        SharedPreferences sp = context.getSharedPreferences(prefsUserId, Context.MODE_PRIVATE);
        return sp.getString(userId, "-1");
    }

    public static void updateCartId(Context context, String id) {
        SharedPreferences sp = context.getSharedPreferences(prefsCartId, Context.MODE_PRIVATE);
        sp.edit().putString(cartId, id).apply();
    }

    public static String getCartId(Context context) {
        SharedPreferences sp = context.getSharedPreferences(prefsCartId, Context.MODE_PRIVATE);
        return sp.getString(cartId, "0");

    }


    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager mConMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return mConMgr.getActiveNetworkInfo() != null
                && mConMgr.getActiveNetworkInfo().isAvailable()
                && mConMgr.getActiveNetworkInfo().isConnected();
    }


    public static void setProgressDialog(Context context) {

        int llPadding = 30;
        LinearLayout ll = new LinearLayout(context);
        ll.setOrientation(LinearLayout.HORIZONTAL);
        ll.setPadding(llPadding, llPadding, llPadding, llPadding);
        ll.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams llParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        llParam.gravity = Gravity.CENTER;
        ll.setLayoutParams(llParam);

        ProgressBar progressBar = new ProgressBar(context);
        progressBar.setIndeterminate(true);
        progressBar.setPadding(0, 0, llPadding, 0);
        progressBar.setLayoutParams(llParam);

        llParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        llParam.gravity = Gravity.CENTER;
        TextView tvText = new TextView(context);
        tvText.setText("Loading ...");
        tvText.setTextColor(Color.parseColor("#000000"));
        tvText.setTextSize(20);
        tvText.setLayoutParams(llParam);

        ll.addView(progressBar);
        ll.addView(tvText);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(ll);

        dialog = builder.create();
        dialog.show();
        Window window = dialog.getWindow();
        if (window != null) {
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(dialog.getWindow().getAttributes());
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT;
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setAttributes(layoutParams);
        }
        }

    public static void disappearProgressDialog()
    {
        dialog.dismiss();
    }

    public static void alertDialog(Context context,String message)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(message);
        dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });
        dialog.show();

    }


}
