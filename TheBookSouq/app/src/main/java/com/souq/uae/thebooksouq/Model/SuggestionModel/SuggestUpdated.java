package com.souq.uae.thebooksouq.Model.SuggestionModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuggestUpdated {
    @SerializedName("suggest")
    @Expose
    private List<String> suggest = null;

    public List<String> getSuggest() {
        return suggest;
    }

    public void setSuggest(List<String> suggest) {
        this.suggest = suggest;
    }
}
