package com.souq.uae.thebooksouq.Adapt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.souq.uae.thebooksouq.Activity.BookDescriptionActivity;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.Datum;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HorizonAdapt extends RecyclerView.Adapter <HorizonAdapt.HorizonAdaptViewHolder>{

  // List<HorizonModel> horizonModels ;
    private List<Datum> datumList;
    Context context;


    public HorizonAdapt(Context context,   /* List<HorizonModel> horizonModels*/ List<Datum> datumList )
    {
        this.context = context;
        this.datumList = datumList;
    }

    @NonNull
    @Override
    public HorizonAdaptViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.horizontal_list_item,viewGroup,false);
            HorizonAdaptViewHolder horizonAdaptViewHolder = new HorizonAdaptViewHolder(view);
        return horizonAdaptViewHolder;

    }

    @Override
    public void onBindViewHolder(HorizonAdaptViewHolder viewHolder, int i) {

        //if (datumList.size()>0) {
            viewHolder.tv_itemName.setText(datumList.get(i).getTitle());
            Picasso.get().load(Utils.HOME_BASE_URL_IMAGE + datumList.get(i).getImageUrl()).placeholder(R.drawable.image_placeholder).into(viewHolder.imv_itemImage);
            //viewHolder.imv_itemImage.setImageResource(R.drawable.icons8_notification_50);
            viewHolder.imv_cartImage.setImageResource(R.drawable.icons8_shopping_cart_filled_50);
            if (datumList.get(i).getDiscount_price()>0) {
                viewHolder.tv_itemPrice.setText(String.valueOf(datumList.get(i).getPrice()) + " " + "AED");
                viewHolder.tv_itemPrice.setPaintFlags(viewHolder.tv_itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_discountedItemPrice.setText(String.valueOf(datumList.get(i).getDiscount_price()) + " " + "AED");
                viewHolder.tv_discountedItemPrice.setTextColor(context.getResources().getColor(R.color.black));
            }

            else {
                viewHolder.tv_itemPrice.setVisibility(View.INVISIBLE);
              //  viewHolder.tv_itemPrice.setPaintFlags(viewHolder.tv_itemPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.tv_discountedItemPrice.setText(String.valueOf(datumList.get(i).getPrice()) + " " + "AED");
                viewHolder.tv_discountedItemPrice.setTextColor(context.getResources().getColor(R.color.black));
            }
            //viewHolder.itemView.setTag(i);
       // }
        }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }

    public class HorizonAdaptViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tv_itemName,tv_itemPrice,tv_discountedItemPrice;
        ImageView imv_itemImage,imv_cartImage;

        public HorizonAdaptViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_itemName = itemView.findViewById(R.id.tv_item_title);
            imv_itemImage = itemView.findViewById(R.id.imgv_cover);
            imv_cartImage = itemView.findViewById(R.id.imgv_cart);
            tv_itemPrice = itemView.findViewById(R.id.tv_item_price);
            tv_discountedItemPrice = itemView.findViewById(R.id.tv_item_discounted_price);

            imv_itemImage.setTag(R.integer.btn_itemImage_view);
            imv_cartImage.setTag(R.integer.btn_addToCart_view);

            imv_itemImage.setOnClickListener(this);
            imv_cartImage.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch(view.getId())
            {
                case R.id.imgv_cover:
                    int tag =(int) view.getTag();
                //    Toast.makeText(context, String.valueOf(tag), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(context, BookDescriptionActivity.class);
                     intent.putExtra(Utils.BOOK_ID,String.valueOf(datumList.get(getAdapterPosition()).getId()));
                    context.startActivity(intent);
                    /* BookDescriptionFragment bookDescriptionFragment= new BookDescriptionFragment();
                    HomeActivity homeActivity = (HomeActivity)context;
                    FragmentTransaction fragmentTransaction = homeActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_home, bookDescriptionFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();*/
                    break;

                case R.id.imgv_cart:
                   // Toast.makeText(context, "cart", Toast.LENGTH_SHORT).show();

                    int cartCount = Utils.getCartCount(context);
                    Utils.saveCartCount(context,++cartCount);
                    Activity activity =(Activity)context;
                    activity.invalidateOptionsMenu();
                    break;
            }
        }
    }

}
