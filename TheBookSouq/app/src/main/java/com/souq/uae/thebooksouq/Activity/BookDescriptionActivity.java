package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Adapt.BookDescriptionAdapter;
import com.souq.uae.thebooksouq.Adapt.SearchSuggestionsAdapter;
import com.souq.uae.thebooksouq.Adapt.ViewPagerAdapter;
import com.souq.uae.thebooksouq.Model.AddToCart.AddToCart;
import com.souq.uae.thebooksouq.Model.BookDescription.BookDescription;
import com.souq.uae.thebooksouq.Model.BookDescription.BookDetail;
import com.souq.uae.thebooksouq.Model.SuggestionModel.SuggestUpdated;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookDescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private LinearLayout linearLayout;
    private Toolbar toolbar;
    private TextView  /*tv_alreadyincart,*/textCartItemCount, tv_bookAuthor, tv_bookPrice, tv_gener, tv_condition, tv_merchnat, tv_bookTitle, tv_description/*, tv_plus, tv_minus*/, tv_quantity, tv_publsiher, tv_discountedPrice;
    private Button tv_alreadyincart, btn_addToCart, btn_outOfStock;
    private ProgressBar progressBar, progressBar1;
    private ImageButton imgbtn_plus, imgbtn_minus;
    private String bookId;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private CirclePageIndicator circlePageIndicator;
    private static int quant = 1;
    private static int NUM_PAGES = 0;
    private String userId;
    private String cartId;
    private Map<String, String> map;
    private AppBarLayout appBarLayout;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_description);
        findViews();


    }

    private void findViews() {
       recyclerView = findViewById(R.id.abd_reyclerview);
        recyclerView.setLayoutManager((new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)));
        userId = Utils.getUserId(BookDescriptionActivity.this);
        cartId = Utils.getCartId(BookDescriptionActivity.this);
        tv_publsiher = findViewById(R.id.abd_tv_publisher);
        imgbtn_plus = findViewById(R.id.abd_imgbtn_plus);
        imgbtn_minus = findViewById(R.id.abd_imgbtn_minus);
        tv_quantity = findViewById(R.id.abd_tv_quantity);
        btn_addToCart = findViewById(R.id.abd_btn_addtocart);
        linearLayout = findViewById(R.id.abd_ll);
        tv_bookTitle = findViewById(R.id.abd_tv_title);
        tv_bookAuthor = findViewById(R.id.abd_tv_author);
        tv_bookPrice = findViewById(R.id.abd_tv_price);
        tv_gener = findViewById(R.id.abd_tv_gener);
        tv_condition = findViewById(R.id.abd_tv_condition);
        tv_merchnat = findViewById(R.id.abd_tv_merchant);
        tv_description = findViewById(R.id.abd_tv_description);
        tv_discountedPrice = findViewById(R.id.abd_tv_discounted_price);
        btn_outOfStock = findViewById(R.id.abd_btn_outofStock);
        viewPager = findViewById(R.id.abd_view_pager);
        circlePageIndicator = findViewById(R.id.abd_indicator);
        progressBar = findViewById(R.id.abd_progressbar);
        tv_alreadyincart = findViewById(R.id.abd_tv_in_cart);
        appBarLayout = findViewById(R.id.appbar);
        bookId = getIntent().getStringExtra(Utils.BOOK_ID);
        toolbar = findViewById(R.id.toolbar_activity_book_description);
        toolbar.setTitle(" ");
        progressBar1 = findViewById(R.id.progressBar1);
        relativeLayout = findViewById(R.id.rl_plus_minus);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        tv_alreadyincart.setOnClickListener(this);
        btn_addToCart.setOnClickListener(this);
        imgbtn_plus.setOnClickListener(this);
        imgbtn_minus.setOnClickListener(this);
        /*addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int cartCount = Utils.getCartCount(BookDescriptionActivity.this);
                Utils.saveCartCount(BookDescriptionActivity.this,++cartCount);
                invalidateOptionsMenu();
            }
        });*/

        //  getBookDetails(bookId,cartId);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.abd_btn_addtocart:
                /*int cartCount = Utils.getCartCount(BookDescriptionActivity.this);
                Utils.saveCartCount(BookDescriptionActivity.this, ++cartCount);
                invalidateOptionsMenu();*/
                userId = Utils.getUserId(BookDescriptionActivity.this);
                cartId = Utils.getCartId(BookDescriptionActivity.this);
                onClickAddToCart(cartId, userId, bookId, tv_quantity.getText().toString());
                break;

            case R.id.abd_imgbtn_plus:

                quant = Integer.valueOf(tv_quantity.getText().toString());
                if (quant < 5) {
                    quant = quant + 1;
                    tv_quantity.setText(String.valueOf(quant));
                }
                break;

            case R.id.abd_imgbtn_minus:
                quant = Integer.valueOf(tv_quantity.getText().toString());
                if (quant > 1) {
                    quant = quant - 1;
                    tv_quantity.setText(String.valueOf(quant));
                }
                break;


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_book_description, menu);
        final MenuItem menuItem = menu.findItem(R.id.menu_cart);
        if (!Utils.getToken(BookDescriptionActivity.this).equalsIgnoreCase("0")) {
            MenuItem menu_notification = menu.findItem(R.id.menu_notification);
            MenuItem menu_profile = menu.findItem(R.id.menu_signin);
            menu_notification.setVisible(true);
            menu_profile.setVisible(false);
            //invalidateOptionsMenu();
        }

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.cart_badge);
        setupBadge(Utils.getCartCount(this));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;

            case R.id.menu_cart:
                Intent intent = new Intent(this, CartActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_signin:
                Toast.makeText(this, "signIn", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this, RegistrationActivity.class);
                startActivity(intent1);
                break;
            case R.id.menu_notification:
                Toast.makeText(this, "bell", Toast.LENGTH_SHORT).show();
                break;
            case R.id.menu_home:
                Intent intent2 = new Intent(this,HomeActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent2);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupBadge(int cartItems) {

        if (textCartItemCount != null) {
            if (cartItems == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(cartItems, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void getBookDetails(final String id, final String cartId) {

        if (Utils.isInternetAvailable(this)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getBookDetails(id, cartId).enqueue(new Callback<BookDescription>() {
                @Override
                public void onResponse(Call<BookDescription> call, Response<BookDescription> response) {
                    if (response.body() != null) {
                        BookDetail bookDetail = response.body().getBookDetails().get(0);
                        if (bookDetail.getStock() > 0) {
                            imgbtn_plus.setVisibility(View.VISIBLE);
                            imgbtn_minus.setVisibility(View.VISIBLE);
                            tv_quantity.setText("1");
                            btn_addToCart.setVisibility(View.VISIBLE);
                            btn_outOfStock.setVisibility(View.GONE);
                        }

                        if (bookDetail.getStock() < 1) {
                            relativeLayout.setVisibility(View.GONE);
                            btn_outOfStock.setVisibility(View.VISIBLE);
                        }
                        if (bookDetail.getBookStatus().equals("already added")) {
                            btn_outOfStock.setVisibility(View.GONE);
                            btn_addToCart.setVisibility(View.GONE);
                            relativeLayout.setVisibility(View.GONE);
                            tv_alreadyincart.setVisibility(View.VISIBLE);
                        }
                        tv_publsiher.setText(bookDetail.getBookPublisher());
                        tv_bookAuthor.setText("by" + " " + bookDetail.getAuthor());
                        tv_bookTitle.setText(bookDetail.getTitle());
                        if (bookDetail.getDiscountPrice()>0) {
                            tv_bookPrice.setText(String.valueOf(bookDetail.getPrice()) + " " + "AED");
                            tv_bookPrice.setPaintFlags(tv_bookPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                            tv_bookPrice.setTextColor(getResources().getColor(R.color.quantity_text));
                            tv_discountedPrice.setText(String.valueOf(bookDetail.getDiscountPrice()) + " " + "AED");
                            tv_discountedPrice.setTextColor(getResources().getColor(R.color.black));
                        } else {
                            tv_bookPrice.setVisibility(View.GONE);
                            tv_discountedPrice.setText(String.valueOf(bookDetail.getPrice()) + " " + "AED");
                            tv_discountedPrice.setTextColor(getResources().getColor(R.color.black));
                        }

                        tv_condition.setText(bookDetail.getCondition());
                        tv_gener.setText(bookDetail.getCategory());
                        tv_merchnat.setText(bookDetail.getMerchantName());
                        tv_description.setText(bookDetail.getDescription());

                        List<String> imageUrlList = new ArrayList<>();

                        Toast.makeText(BookDescriptionActivity.this, Utils.HOME_BASE_URL_IMAGE + bookDetail.getFrontImage(), Toast.LENGTH_SHORT).show();
                        imageUrlList.add(Utils.HOME_BASE_URL_IMAGE + bookDetail.getFrontImage());
                        imageUrlList.add(Utils.HOME_BASE_URL_IMAGE + bookDetail.getFirstImage());
                        imageUrlList.add(Utils.HOME_BASE_URL_IMAGE + bookDetail.getBackImage());

                        viewPagerAdapter = new ViewPagerAdapter(BookDescriptionActivity.this, imageUrlList);
                        viewPager.setAdapter(viewPagerAdapter);
                        circlePageIndicator.setViewPager(viewPager);
                        final float density = getResources().getDisplayMetrics().density;
                        circlePageIndicator.setRadius(3 * density);
                        NUM_PAGES = imageUrlList.size();
                        BookDescriptionAdapter bookDescriptionAdapter = new BookDescriptionAdapter(BookDescriptionActivity.this,response.body().getMoreLikeThis());
                        recyclerView.setAdapter(bookDescriptionAdapter);
                        progressBar.setVisibility(View.GONE);
                        appBarLayout.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {
                        Utils.alertDialog(BookDescriptionActivity.this, getResources().getString(R.string.no_book_found));
                    }
                }

                @Override
                public void onFailure(Call<BookDescription> call, Throwable t) {
                    t.printStackTrace();

                }
            });
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(BookDescriptionActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   getBookDetails(id, cartId);
                }
            });
            dialog.show();
        }


    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        btn_addToCart.setVisibility(View.GONE);
        btn_outOfStock.setVisibility(View.GONE);
        tv_alreadyincart.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        userId = Utils.getUserId(BookDescriptionActivity.this);
        cartId = Utils.getCartId(BookDescriptionActivity.this);
        getBookDetails(bookId, cartId);

        super.onResume();
    }

    private void onClickAddToCart(final String cartId,final String userId,final String bookId,final String quantity) {

        if (Utils.isInternetAvailable(this)) {
            progressBar1.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            map = new HashMap<>();
            map.put("quantity", quantity);
            map.put("book_id", bookId);
            if (!userId.equals("-1")) {
                map.put("user_id", userId);
            }
            if (!(cartId.equals("-1") || cartId.equals("0"))) {


                map.put("cart_id", cartId);
            }
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().addToCart(map).enqueue(new Callback<AddToCart>() {
                @Override
                public void onResponse(Call<AddToCart> call, Response<AddToCart> response) {

                    if (response.body() != null) {
                        if (response.body().getStatus() == 200) {
                            if (!(response.body().getCart_quantity() == null)) {
                                Utils.saveCartCount(BookDescriptionActivity.this, Integer.valueOf(response.body().getCart_quantity()));
                                invalidateOptionsMenu();
                            }
                            Utils.updateCartId(BookDescriptionActivity.this, String.valueOf(response.body().getCartId()));
                            Utils.updateUserId(BookDescriptionActivity.this, String.valueOf(response.body().getUserId()));
                            //Toast.makeText(BookDescriptionActivity.this, response.body().getBook_status(), Toast.LENGTH_SHORT).show();
                            btn_addToCart.setVisibility(View.GONE);

                            tv_alreadyincart.setVisibility(View.VISIBLE);
                            progressBar1.setVisibility(View.GONE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            if (!(response.body().getBook_status().equalsIgnoreCase("added succesfully"))) {
                                relativeLayout.setVisibility(View.GONE);
                                tv_alreadyincart.setVisibility(View.GONE);
                                btn_outOfStock.setVisibility(View.VISIBLE);
                            }
                            if (!(response.body().getMessage().equalsIgnoreCase("Added SuccessFully"))) {
                                Utils.alertDialog(BookDescriptionActivity.this, response.body().getMessage());
                            }
                            relativeLayout.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<AddToCart> call, Throwable t) {
                    t.printStackTrace();

                }
            });
        }else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(BookDescriptionActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onClickAddToCart(cartId, userId, bookId, quantity);
                }
            });
            dialog.show();
        }


    }
}
