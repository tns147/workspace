package com.souq.uae.thebooksouq.Network;

import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginRetrofitClass {

    private static LoginRetrofitClass instance = null;
    private static LoginRetrofitClass instance2 =null;
    private static LoginRetrofitClass instance3 =null;
    private static WebRequestGeo service;
    private static WebRequestGeo service2;
    private static WebRequestGeo service3;
    private LoginRetrofitClass() {
        // Exists only to defeat instantiation.
    }

    public static LoginRetrofitClass getGeoInstance() {
        if (instance == null) {
            instance = new LoginRetrofitClass();
            OkHttpClient.Builder client = new OkHttpClient.Builder().connectTimeout( 5, TimeUnit.MINUTES ).readTimeout( 5, TimeUnit.MINUTES ).writeTimeout( 5, TimeUnit.MINUTES ).build().newBuilder();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY );

            client.addInterceptor( loggingInterceptor );
            Retrofit loginRetrofit = new Retrofit.Builder().baseUrl(Utils.LOCATION_IQ_URL).client( client.build() ).addConverterFactory( GsonConverterFactory.create() ).build();
            service = loginRetrofit .create( WebRequestGeo.class );

            service = loginRetrofit .create( WebRequestGeo.class );

        }
        return instance;
    }
    public static LoginRetrofitClass getHomeInstance() {
        if (instance2 == null) {
            instance2 = new LoginRetrofitClass();
            OkHttpClient.Builder client = new OkHttpClient.Builder().connectTimeout( 5, TimeUnit.MINUTES ).readTimeout( 5, TimeUnit.MINUTES ).writeTimeout( 5, TimeUnit.MINUTES ).build().newBuilder();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY );

            client.addInterceptor( loggingInterceptor );
            Retrofit loginRetrofit = new Retrofit.Builder().baseUrl(Utils.HOME_BASE_URL).client( client.build() ).addConverterFactory( GsonConverterFactory.create() ).build();
            service2 = loginRetrofit .create( WebRequestGeo.class );

            service2 = loginRetrofit .create( WebRequestGeo.class );

        }
        return instance2;
    }

    public static LoginRetrofitClass getTwilioInstance() {
        if (instance3 == null) {
            instance3 = new LoginRetrofitClass();
            OkHttpClient.Builder client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.MINUTES).readTimeout(5, TimeUnit.MINUTES).writeTimeout(5, TimeUnit.MINUTES).build().newBuilder();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            client.addInterceptor(loggingInterceptor);
            Retrofit loginRetrofit = new Retrofit.Builder().baseUrl(Utils.TWILLIO_URL).client( client.build() ).addConverterFactory( GsonConverterFactory.create() ).build();
            service3 = loginRetrofit .create( WebRequestGeo.class );

            service3 = loginRetrofit .create( WebRequestGeo.class );        }
            return instance3;
    }

    public WebRequestGeo getWebRequestsInstance() {

        return service;
    }
    public WebRequestGeo getWebRequestsInstance2() {

        return service2;
    }

    public  WebRequestGeo getTwillioService() {
        return service3;
    }
}
