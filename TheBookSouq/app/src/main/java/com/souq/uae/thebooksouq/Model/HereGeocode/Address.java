package com.souq.uae.thebooksouq.Model.HereGeocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Address {

    @SerializedName("Label")
    @Expose
    private String label;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("County")
    @Expose
    private String county;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("District")
    @Expose
    private String district;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("HouseNumber")
    @Expose
    private String houseNumber;
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;
    @SerializedName("AdditionalData")
    @Expose
    private List<AdditionalDatum> additionalData = null;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<AdditionalDatum> getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(List<AdditionalDatum> additionalData) {
        this.additionalData = additionalData;
    }

}