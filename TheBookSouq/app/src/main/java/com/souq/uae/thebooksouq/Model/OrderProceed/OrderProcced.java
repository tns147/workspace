package com.souq.uae.thebooksouq.Model.OrderProceed;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProcced {

    @SerializedName("cash_payment_allowed")
    @Expose
    private boolean cash_payment_allowed;

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total_price")
    @Expose
    private Double totalPrice;
    @SerializedName("expected_delivery_date")
    @Expose
    private String expectedDeliveryDate;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public boolean isCash_payment_allowed() {
        return cash_payment_allowed;
    }

    public void setCash_payment_allowed(boolean cash_payment_allowed) {
        this.cash_payment_allowed = cash_payment_allowed;
    }
}