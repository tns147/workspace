package com.souq.uae.thebooksouq.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.souq.uae.thebooksouq.Fragments.CartFragment;
import com.souq.uae.thebooksouq.Fragments.HomeFragment;

import com.souq.uae.thebooksouq.Model.GeoModels.MainObject;
import com.souq.uae.thebooksouq.Model.HereGeocode.ReverseGeoCoding;
import com.souq.uae.thebooksouq.Model.LocationIQ.LocationIqReverseGeocoding;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest = new LocationRequest();
    private String longitude, latitude;
    private ImageButton img_gps;
    final List<String> locations = new ArrayList<>();
    private final String TAG = HomeActivity.class.getSimpleName();
    private boolean check = true;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView textCartItemCount;
    private MaterialSpinner materialSpinner;
    private AlertDialog dialog ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
       // processExtrasData();
        FirebaseMessaging.getInstance().subscribeToTopic("TheBookSouqConsumer");
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( HomeActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Utils.setFirebaseToken(HomeActivity.this,newToken);

                Log.e("FirebaseToken",newToken);

            }
        });

        findViews();
        setNavigationDrawer();

        if (savedInstanceState == null) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frame_home, homeFragment);
            fragmentTransaction.commit();
        }
        /*int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (rc == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
                ActivityCompat.requestPermissions(this, permissions,8);
                return;
            }
        }*/


    }

/*
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
       //must store the new intent unless getIntent() will return the old one
        processExtrasData();
    }
*/

    /*private void processExtrasData() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String myInfo= extras.getString("myInfo", "I didn't found any info");

            Log.d("MyApp", myInfo);
        }
    }*/

    private void findViews() {
        img_gps = findViewById(R.id.home_img_location);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(" ");
        drawerLayout = findViewById(R.id.drawer_layout);
        materialSpinner = findViewById(R.id.home_spinner);
        locations.add(Utils.getLocation(HomeActivity.this));
        //materialSpinner.setAnimation(null);
        materialSpinner.setItems(locations);
        materialSpinner.setMaxLines(1);
        materialSpinner.setEllipsize(TextUtils.TruncateAt.END);
        materialSpinner.setTextSize(14);
        materialSpinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                /*                if (item.toString().equalsIgnoreCase("Get Current Location")) {*/
              //  Toast.makeText(HomeActivity.this, item.toString(), Toast.LENGTH_SHORT).show();


                      /*  String ul = "Updated Location";

                        materialSpinner.setText(ul);*/

                /*    mLocationClient = null;
                    check = true;
                    showSetting();*/
                //materialSpinner.setItems("");
/*

                }
*/

            }
        });

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.nav_blue);
        img_gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLocationClient = null;
                check = true;
                showSetting();
            }
        });
    }

    private void setNavigationDrawer() {
        NavigationView navigationView = findViewById(R.id.navigation);
        navigationView.setItemIconTintList(null);

        if (Utils.getToken(HomeActivity.this).equalsIgnoreCase("-1")|| Utils.getToken(HomeActivity.this).equalsIgnoreCase("0"))
        {
            navigationView.getMenu().findItem(R.id.dmenu_orders).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int itemId = menuItem.getItemId();

                if (itemId == R.id.dmenu_home) {

                    drawerLayout.closeDrawers();
                    startActivity(new Intent(HomeActivity.this, HomeActivity.class));
                    overridePendingTransition(0, 0);
                    HomeActivity.this.finish();
                    overridePendingTransition(0, 0);

                }
                if (itemId == R.id.dmenu_profile) {

                } else if (itemId == R.id.dmenu_cart) {
                    drawerLayout.closeDrawers();
                    Intent intent = new Intent(HomeActivity.this, CartActivity.class);
                    startActivity(intent);
                } else if (itemId == R.id.dmenu_orders) {
                    drawerLayout.closeDrawers();
                    Intent intent = new Intent(HomeActivity.this, MyOrdersActivity.class);
                    startActivity(intent);


                } else if (itemId == R.id.dmenu_how_works) {

                } else if (itemId == R.id.dmenu_help) {

                } else if (itemId == R.id.dmenu_about) {

                }

                return false;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        final MenuItem menuItem = menu.findItem(R.id.menu_cart);
        MenuItem menu_home = menu.findItem(R.id.menu_home);
        menu_home.setVisible(false);
        if (!Utils.getToken(HomeActivity.this).equalsIgnoreCase("0")) {
            MenuItem menu_notification = menu.findItem(R.id.menu_notification);
            MenuItem menu_profile = menu.findItem(R.id.menu_signin);
            menu_notification.setVisible(true);
            menu_profile.setVisible(false);
            //invalidateOptionsMenu();
        }

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);


        setupBadge(Utils.getCartCount(this));

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.menu_cart:
                Intent intent = new Intent(this, CartActivity.class);
                startActivity(intent);
                break;

            case R.id.menu_signin:
               // Toast.makeText(this, "signIn", Toast.LENGTH_SHORT).show();
                Intent intent1 = new Intent(this, RegistrationActivity.class);
                startActivity(intent1);
                break;
            case R.id.menu_notification:
              //  Toast.makeText(this, "bell", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupBadge(int cartItems) {

        if (textCartItemCount != null) {
            if (cartItems == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(cartItems, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        mLocationClient = null;
        check = true;
        showSetting();

        super.onResume();
    }

    private void geoCodeAddress(final String latitude, final String longitude) {

        if (Utils.isInternetAvailable(HomeActivity.this)) {
            Map<String, String> data = new HashMap<>();
            data.put("key", Utils.LOCATION_IQ_KEY);
            data.put("lat", latitude);
            data.put("lon", longitude);
            data.put("format","json");

            LoginRetrofitClass.getGeoInstance().getWebRequestsInstance().locationIqReverseGeocoding(data).enqueue(new Callback<LocationIqReverseGeocoding>() {
                @Override
                public void onResponse(Call<LocationIqReverseGeocoding> call, Response<LocationIqReverseGeocoding> response) {
                    Log.d("Homefragment", "Responded");


                    if (response.body() != null) {
                         //   Toast.makeText(HomeActivity.this, "Responded", Toast.LENGTH_SHORT).show();
                            Log.d("HomeActivity", "geocodebody not null");


                                        if (response.body().getDisplayName()!=null ) {
                                            String addr = response.body().getDisplayName();
                                            Utils.updateLocation(HomeActivity.this, addr);
                                            materialSpinner.setText(Utils.getLocation(HomeActivity.this));
                                        } else {
                                            materialSpinner.setText("Location, Could Not be updated");
                                        }
                                }
                }
                @Override
                public void onFailure(Call<LocationIqReverseGeocoding> call, Throwable t) {
                    t.printStackTrace();
                   // Toast.makeText(HomeActivity.this, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    geoCodeAddress(latitude,longitude);
                }
            });
            dialog.show();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Error On Connection Failed!");
       // Toast.makeText(this, "No Internet Connectivity Address failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions((Activity) this, permissions, 8);


            Log.d(TAG, "Error On Connection Permission Not Granted!");
            //Toast.makeText(this, "Please Allow Access to Location", Toast.LENGTH_SHORT).show();

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
            }
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        Log.d(TAG, "Connected to Google API");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location changed");

        if (location != null) {
            Log.d(TAG, "== location != null");
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            if (check) {
                geoCodeAddress(latitude, longitude);
                check = false;
            }
        }
    }

    private void getLocation() {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);

        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;

        mLocationRequest.setPriority(priority);
        mLocationClient.connect();
    }

    private void showSetting() {
        if (mLocationClient == null) {
            mLocationClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mLocationClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mLocationClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            getLocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                status.startResolutionForResult(
                                        HomeActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {

            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        showSetting();
                        break;
                }
                break;
        }
    }


}
