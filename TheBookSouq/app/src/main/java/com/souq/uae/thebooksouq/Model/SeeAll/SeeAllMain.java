package com.souq.uae.thebooksouq.Model.SeeAll;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SeeAllMain {

    @SerializedName("books")
    @Expose
    private Books books;
    @SerializedName("total-size")
    @Expose
    private Integer totalSize;
    @SerializedName("page")
    @Expose
    private Integer page;

    public Books getBooks() {
        return books;
    }

    public void setBooks(Books books) {
        this.books = books;
    }

    public Integer getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Integer totalSize) {
        this.totalSize = totalSize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
