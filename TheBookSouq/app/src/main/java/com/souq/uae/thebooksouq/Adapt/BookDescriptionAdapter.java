package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Activity.BookDescriptionActivity;
import com.souq.uae.thebooksouq.Model.BookDescription.MoreLikeThis;
import com.souq.uae.thebooksouq.Model.MyOrders.OrderDetail_;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

public class BookDescriptionAdapter extends RecyclerView.Adapter {
private Context context;
private MoreLikeThis moreLikeThis;
public BookDescriptionAdapter(Context context, MoreLikeThis moreLikeThis) {
        this.context = context;
        this.moreLikeThis = moreLikeThis;
        }

@NonNull
@Override
public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.book_description_horizontal_list, viewGroup, false);
        viewHolder = new BookDescriptionAdapterViewHolder(view);

        return viewHolder;
        }

@Override
public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof BookDescriptionAdapterViewHolder) {
            BookDescriptionAdapterViewHolder vh= ((BookDescriptionAdapterViewHolder)holder);
        vh.tv_name.setText(moreLikeThis.getData().get(i).getTitle());
        if (moreLikeThis.getData().get(i).getDiscountPrice()>0) {
            vh.tv_price.setPaintFlags(vh.tv_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            vh.tv_price.setText(String.valueOf(moreLikeThis.getData().get(i).getPrice()));
            vh.tv_discount_price.setText(String.valueOf(moreLikeThis.getData().get(i).getDiscountPrice())+" "+"AED");
           // vh.tv_discount_price.setTextColor(context.getResources().getColor(R.color.black));
        }
        else {
            vh.tv_price.setVisibility(View.GONE);
            vh.tv_discount_price.setText(String.valueOf(moreLikeThis.getData().get(i).getPrice())+" "+"AED");
           // vh.tv_discount_price.setTextColor(context.getResources().getColor(R.color.black));
        }
        Picasso.get().load(Utils.HOME_BASE_URL_IMAGE+moreLikeThis.getData().get(i).getImageUrl()).into(vh.imageView);

        }
        }

@Override
public int getItemCount() {
        return moreLikeThis.getData().size();
        }

public class BookDescriptionAdapterViewHolder extends RecyclerView.ViewHolder {
    private TextView tv_price,tv_discount_price,tv_name;

    private ImageView imageView;

    public BookDescriptionAdapterViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_price = itemView.findViewById(R.id.bdh_tv_item_price);
        tv_name = itemView.findViewById(R.id.bdh_tv_item_title);
        tv_discount_price = itemView.findViewById(R.id.bdh_tv_item_discounted_price);
        imageView = itemView.findViewById(R.id.bdh_imgv_cover);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookDescriptionActivity.class);
                intent.putExtra(Utils.BOOK_ID,String.valueOf(moreLikeThis.getData().get(getAdapterPosition()).getId()));
                context.startActivity(intent);
            }
        });

    }


}
}

