package com.souq.uae.thebooksouq.Model.HomeFragmentModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainBooks {

    @SerializedName("books")
    @Expose
    private List<Book> books = null;
    @SerializedName("page")
    @Expose
    private Integer page;

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}