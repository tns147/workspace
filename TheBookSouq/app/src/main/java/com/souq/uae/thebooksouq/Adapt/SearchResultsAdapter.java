package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.souq.uae.thebooksouq.Activity.BookDescriptionActivity;
import com.souq.uae.thebooksouq.Model.SearchResults.Book;
import com.souq.uae.thebooksouq.Model.SearchResults.Datum;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchResultsAdapter extends RecyclerView.Adapter{
    private List<Datum> dataList;
    private Context context;


    public SearchResultsAdapter(Context context, List<Datum> dataList)
    {
        this.context = context;
        this.dataList = dataList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_row_item,parent,false);
        RecyclerView.ViewHolder viewHolder = new SearchAdapterViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

       if (holder instanceof  SearchAdapterViewHolder) {
                    Datum datum = dataList.get(position);
                    ((SearchAdapterViewHolder)holder).tv_bookName.setText(datum.getTitle());
                    ((SearchAdapterViewHolder)holder).tv_authorName.setText("by"+" "+datum.getAuthor());
                    //  ((SearchAdapterViewHolder)holder).tv_generName.setText(datum.);
            if (datum.getDiscount_price()>0) {
                ((SearchAdapterViewHolder) holder).tv_discountedPrice.setText(String.valueOf(datum.getDiscount_price())+" "+"AED");
                ((SearchAdapterViewHolder) holder).tv_price.setText(String.valueOf(datum.getPrice()) + " " + "AED");
                ((SearchAdapterViewHolder) holder).tv_price.setPaintFlags(((SearchAdapterViewHolder) holder).tv_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            else {
                ((SearchAdapterViewHolder) holder).tv_discountedPrice.setText(String.valueOf(datum.getPrice())+" "+"AED");
                ((SearchAdapterViewHolder) holder).tv_price.setVisibility(View.INVISIBLE);
            }
           ((SearchAdapterViewHolder)holder).tv_merchantName.setText(datum.getMerchant());
                    Picasso.get().load(Utils.HOME_BASE_URL_IMAGE+datum.getImageUrl()).into(((SearchAdapterViewHolder)holder).imageView);

       }
    }


    @Override
    public int getItemCount() {


            return dataList.size();
        }



    // stores and recycles views as they are scrolled off screen
    public class SearchAdapterViewHolder extends RecyclerView.ViewHolder  {
        ImageView imageView;
        TextView tv_bookName,tv_authorName,tv_merchantName,tv_discountedPrice,tv_price;

        public SearchAdapterViewHolder(View itemView) {
            super(itemView);
            tv_bookName = itemView.findViewById(R.id.search_row_item_bookName);
            tv_authorName = itemView.findViewById(R.id.search_row_item_authorName);
            tv_merchantName = itemView.findViewById(R.id.search_row_item_merchnat);
            tv_price = itemView.findViewById(R.id.search_row_item_price);
            imageView = itemView.findViewById(R.id.search_row_item_bookCover);
            tv_discountedPrice = itemView.findViewById(R.id.search_row_tv_discountedprice);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BookDescriptionActivity.class);
                intent.putExtra(Utils.BOOK_ID,String.valueOf(dataList.get(getAdapterPosition()).getId()));
                context.startActivity(intent);

            }
        });
        }


    }

    public void addDataInSearchResults (List<Datum> datumList)
    {
        Log.v("addBooks", "isLoading=true");
        for (Datum datum : datumList)

        {
            Log.v("SearchResultsAdapt", "book:booksList");

            this.dataList.add(datum);
        }
        notifyDataSetChanged();
    }


}
