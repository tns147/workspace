package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Model.ReviewModel.Datum;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import okhttp3.internal.Util;

public class ViewPagerAdapterOrderReview extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
 //   private Integer[] images = {R.drawable.image2a,R.drawable.image2b,R.drawable.image3a};
    private List<Datum> datumList;

    public ViewPagerAdapterOrderReview(Context context,List<Datum> datumList) {
        this.datumList = datumList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return datumList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view,@NonNull Object object) {
        return view == object;
    }
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        ImageView imageView;
        TextView tv_title,tv_author,tv_price;
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.horizontal_review_viewpager_layout, container, false);
       /* layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.horizontal_review_viewpager_layout, null);*/
        imageView =  view.findViewById(R.id.hrvl_img);
        tv_title = view.findViewById(R.id.hrvl_tv_title);
        tv_author = view.findViewById(R.id.hrvl_tv_author);
        tv_price = view.findViewById(R.id.hrvl_tv_price);
        //imageView.setImageResource(images[position]);
        Picasso.get().load(Utils.HOME_BASE_URL_IMAGE+datumList.get(position).getImage()).into(imageView);
        tv_title.setText(datumList.get(position).getTitle());
        tv_author.setText("by"+" "+datumList.get(position).getAuthor());
        tv_price.setText(String.valueOf(datumList.get(position).getPrice())+" "+"AED");

        container.addView(view);
/*
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
*/
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        /*ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);*/
        container.removeView((View)object);
    }

}
