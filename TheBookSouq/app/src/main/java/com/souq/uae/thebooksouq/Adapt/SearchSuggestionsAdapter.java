package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Activity.SearchrResultsActivity;
import com.souq.uae.thebooksouq.Fragments.SearchResultsFragment;
import com.souq.uae.thebooksouq.Activity.HomeActivity;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.List;

public class SearchSuggestionsAdapter extends RecyclerView.Adapter {

    private List<String> datumList;
    private Context context;

    public SearchSuggestionsAdapter (List<String> datumList, Context context)
    {
        this.datumList = datumList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_suggestions_row,viewGroup,false);
        viewHolder = new SearchSuggestionAdapterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof  SearchSuggestionAdapterViewHolder)
        {
            ((SearchSuggestionAdapterViewHolder)holder).tv_SearchText.setText(datumList.get(i));
         }

    }

    @Override
    public int getItemCount() {
        return datumList.size();
    }



    public class SearchSuggestionAdapterViewHolder extends  RecyclerView.ViewHolder
    {
        private TextView tv_SearchText;
        public SearchSuggestionAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_SearchText = itemView.findViewById(R.id.search_suggestions_tv_row);

            tv_SearchText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent =  new Intent(context, SearchrResultsActivity.class);
                    intent.putExtra(Utils.SEARCH_SUBMIT_KEY,datumList.get(getAdapterPosition()));
                    context.startActivity(intent);

 /*                   SearchResultsFragment searchResultsFragment= new SearchResultsFragment();
                    Bundle args = new Bundle();
                    args.putString(Utils.SEARCH_SUBMIT_KEY,datumList.get(getAdapterPosition()));
                    searchResultsFragment.setArguments(args);
                    HomeActivity homeActivity = (HomeActivity)context;
                    FragmentTransaction fragmentTransaction = homeActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_home, searchResultsFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();*/
                }
            });
        }



    }
}
