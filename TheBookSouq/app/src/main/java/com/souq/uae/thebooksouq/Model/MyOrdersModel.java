package com.souq.uae.thebooksouq.Model;


public class MyOrdersModel {
    private String orderId ;
    private String orderStatus;
    private String orderAmount;
    private String orderAddress;
    private String merchnatName;
    private String deliveryDate;

    public MyOrdersModel(String orderId, String orderAddress, String orderAmount,String orderStatus,String merchnatName,String deliveryDate)
    {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.orderAmount = orderAmount;
        this.orderAddress = orderAddress;
        this.merchnatName = merchnatName;
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getMerchnatName() {
        return merchnatName;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setMerchnatName(String merchnatName) {
        this.merchnatName = merchnatName;
    }
}
