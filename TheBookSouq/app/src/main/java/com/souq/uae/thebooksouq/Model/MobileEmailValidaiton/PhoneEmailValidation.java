package com.souq.uae.thebooksouq.Model.MobileEmailValidaiton;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhoneEmailValidation {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("info")
    @Expose
    private Info info;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

}
