package com.souq.uae.thebooksouq.Model.HereGeocode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MetaInfo {

    @SerializedName("Timestamp")
    @Expose
    private String timestamp;
    @SerializedName("NextPageInformation")
    @Expose
    private String nextPageInformation;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNextPageInformation() {
        return nextPageInformation;
    }

    public void setNextPageInformation(String nextPageInformation) {
        this.nextPageInformation = nextPageInformation;
    }

}