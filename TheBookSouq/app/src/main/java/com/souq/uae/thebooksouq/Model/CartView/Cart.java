package com.souq.uae.thebooksouq.Model.CartView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cart {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("total")
    @Expose
    private List<Total> total = null;
    @SerializedName("cart_quantity")
    @Expose
    private List<CartQuantity> cartQuantity = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public List<Total> getTotal() {
        return total;
    }

    public void setTotal(List<Total> total) {
        this.total = total;
    }

    public List<CartQuantity> getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(List<CartQuantity> cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

}