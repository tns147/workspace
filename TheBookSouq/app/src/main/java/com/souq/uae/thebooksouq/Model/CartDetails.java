package com.souq.uae.thebooksouq.Model;

public class CartDetails {
    String image;
    String bookName;
    String authorName;
    String price;
    String count;

    public CartDetails ( String image, String bookName, String authorName, String price,String count)
    {
        this.image = image;
        this.bookName = bookName;
        this.authorName = authorName;
        this.price = price;
        this.count = count;
    }
    public CartDetails ( String bookName, String authorName, String price,String count)
    {
        this.bookName = bookName;
        this.authorName = authorName;
        this.price = price;
        this.count = count;
    }

    public String getImage() {
        return image;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getBookName() {
        return bookName;
    }

    public String getCount() {
        return count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setCount(String count)
    {
        this.count = count;
    }
}
