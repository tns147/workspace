package com.souq.uae.thebooksouq.Model.TwillioVerification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendSms {

    @SerializedName("carrier")
    @Expose
    private String carrier;
    @SerializedName("is_cellphone")
    @Expose
    private Boolean isCellphone;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("seconds_to_expire")
    @Expose
    private Integer secondsToExpire;
    @SerializedName("uuid")
    @Expose
    private String uuid;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public Boolean getIsCellphone() {
        return isCellphone;
    }

    public void setIsCellphone(Boolean isCellphone) {
        this.isCellphone = isCellphone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSecondsToExpire() {
        return secondsToExpire;
    }

    public void setSecondsToExpire(Integer secondsToExpire) {
        this.secondsToExpire = secondsToExpire;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}