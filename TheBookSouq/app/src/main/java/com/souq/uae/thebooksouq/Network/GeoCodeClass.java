package com.souq.uae.thebooksouq.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GeoCodeClass {

    //private static GeoCodeClass instance = null;
    private static Retrofit retrofit = null;

    public static WebRequestGeo getClient (String baseUrl) {

        // change your base URL
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        //Creating object for our interface
        WebRequestGeo api = retrofit.create(WebRequestGeo.class);
        return api; // return the APIInterface object
    }
}
