package com.souq.uae.thebooksouq.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.souq.uae.thebooksouq.Model.Login.Login;
import com.souq.uae.thebooksouq.Model.MobileEmailValidaiton.PhoneEmailValidation;
import com.souq.uae.thebooksouq.Model.TwillioVerification.SendSms;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private AwesomeValidation mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
    private EditText et_phoneNo;
    private Button btn_submit;
    private String phoneNo;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViews();
        performActions();
    }

    private void findViews() {
        toolbar = findViewById(R.id.toolbar_activity_login);
        et_phoneNo = findViewById(R.id.login_et_phoneno);
        btn_submit = findViewById(R.id.login_btn_submit);

    }

    private void performActions() {


        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //et_phoneNo.setHint("UAE Mobile no with country code");

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phoneNo = et_phoneNo.getText().toString();

                // et_phoneNo.getText().clear();
                // et_phoneNo.setHint(getResources().getString(R.string.phoneno_validity_msg));

                if (phoneNo.length()>8 &&phoneNo.length()<10 && phoneNo.matches("[0-9]{9}")) {
                    Utils.setProgressDialog(LoginActivity.this);
                    mobileEmailValidation(phoneNo, "");
                    //loginFt(phoneNo);
                } else {

                    mAwesomeValidation.addValidation(et_phoneNo, "[0-9]{9}", getResources().getString(R.string.phoneno_validity_msg));

                    mAwesomeValidation.validate();

                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();
        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void mobileEmailValidation(final String mobile, final String email) {
        if (Utils.isInternetAvailable(this)) {
            Map<String, String> data = new HashMap<>();
            data.put("mobile", mobile);
            data.put("email", email);
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().phoneEmailValidation(data).enqueue(new Callback<PhoneEmailValidation>() {
                @Override
                public void onResponse(Call<PhoneEmailValidation> call, Response<PhoneEmailValidation> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("ok")) {
                            if (response.body().getInfo() != null) {
                                //String email = response.body().getInfo().getEmail();
                                String phoneNo = response.body().getInfo().getPhoneNumber();
                                if (phoneNo.equalsIgnoreCase("yes")) {
                                    twillioSendSms(mobile);
                                } else {
                                    Utils.disappearProgressDialog();
                                    Utils.alertDialog(LoginActivity.this, getString(R.string.phone_no_not_exist));
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<PhoneEmailValidation> call, Throwable t) {
                    Utils.disappearProgressDialog();
                    t.printStackTrace();
                }
            });
        } else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    mobileEmailValidation(mobile, email);
                }
            });
            dialog.show();
        }
    }

    private void twillioSendSms(final String phoneNo) {
        Map<String, String> data = new HashMap<>();
        data.put("via", "sms");
        data.put("phone_number", phoneNo);
        data.put("country_code", Utils.COUNTRY_CODE);
        if (Utils.isInternetAvailable(LoginActivity.this)) {

            LoginRetrofitClass.getTwilioInstance().getTwillioService().sendSms(Utils.TWILLIO_API_KEY, data).enqueue(new Callback<SendSms>() {
                @Override
                public void onResponse(Call<SendSms> call, Response<SendSms> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            Utils.disappearProgressDialog();
                          /*  Intent intent = new Intent(ValidationActivity.this, ValidationActivity.class);
                            intent.putExtra("mobile",phoneNo);
                            startActivity(intent);*/
                            dataTransfer();

                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(LoginActivity.this, response.body().getMessage());
                        }
                    } else {
                        Utils.disappearProgressDialog();
                        Utils.alertDialog(LoginActivity.this, getString(R.string.invalid_no));
                    }
                }

                @Override
                public void onFailure(Call<SendSms> call, Throwable t) {
                    Utils.disappearProgressDialog();
                    t.printStackTrace();
                }
            });
        } else {
            Utils.disappearProgressDialog();

            AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    twillioSendSms(phoneNo);
                }
            });
            dialog.show();
        }
    }

    private void dataTransfer() {
        Intent intent = new Intent(this, ValidationActivity.class);
        intent.putExtra("mobile", phoneNo);
        intent.putExtra("activity", "login");
        startActivity(intent);
    }
}
