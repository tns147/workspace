package com.souq.uae.thebooksouq.Model.AddToCart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddToCart {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("cart_id")
    @Expose
    private Integer cartId;

    @SerializedName("book_status")
    @Expose
    private String book_status;

    @SerializedName("quantity_added")
    @Expose
    private String quantity_added;

    @SerializedName("book_stock")
    @Expose
    private String book_stock;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("cart_quantity")
    @Expose
    private String cart_quantity;

    public void setCart_quantity(String cart_quantity) {
        this.cart_quantity = cart_quantity;
    }

    public String getCart_quantity() {
        return cart_quantity;
    }

    public void setQuantity_added(String quantity_added) {
        this.quantity_added = quantity_added;
    }

    public String getQuantity_added() {
        return quantity_added;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setBook_stock(String book_stock) {
        this.book_stock = book_stock;
    }

    public String getBook_stock() {
        return book_stock;
    }

    public String getBook_status() {
        return book_status;
    }

    public void setBook_status(String book_status) {
        this.book_status = book_status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

}