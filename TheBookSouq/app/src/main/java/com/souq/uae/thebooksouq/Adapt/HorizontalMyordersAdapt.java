package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Model.MyOrders.OrderDetail_;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

public class HorizontalMyordersAdapt extends RecyclerView.Adapter {
    private Context context;
    private OrderDetail_ orderDetailUnder;
    public HorizontalMyordersAdapt(Context context, OrderDetail_ orderDetailUnder) {
        this.context = context;
        this.orderDetailUnder = orderDetailUnder;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_orders_horizontal, viewGroup, false);
        viewHolder = new HorizonOrdersViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof HorizonOrdersViewHolder) {
            HorizonOrdersViewHolder vh= ((HorizonOrdersViewHolder)holder);
            vh.tv_quantity.setText(" 99 ");

            Picasso.get().load(Utils.HOME_BASE_URL_IMAGE+orderDetailUnder.getBookImage().get(0)).into(vh.imageView);
//            vh.imageView.setImageResource(R.drawable.image_placeholder);
        }
    }

    @Override
    public int getItemCount() {
        return orderDetailUnder.getBookImage().size();
    }

    public class HorizonOrdersViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_quantity;
        private ImageView imageView;

        public HorizonOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_quantity = itemView.findViewById(R.id.moh_tv);
            imageView = itemView.findViewById(R.id.moh_imgv);
        }
    }
}
