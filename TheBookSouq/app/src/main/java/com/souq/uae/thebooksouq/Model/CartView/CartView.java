package com.souq.uae.thebooksouq.Model.CartView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartView {

    @SerializedName("cart")
    @Expose
    private Cart cart;

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

}