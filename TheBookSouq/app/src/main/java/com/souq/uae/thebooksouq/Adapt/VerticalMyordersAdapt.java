package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Model.Login.Login;
import com.souq.uae.thebooksouq.Model.MyOrders.MyOrders;
import com.souq.uae.thebooksouq.Model.MyOrders.OrderDetail;
import com.souq.uae.thebooksouq.Model.MyOrders.OrderDetail_;
import com.souq.uae.thebooksouq.R;

public class VerticalMyordersAdapt extends RecyclerView.Adapter {

    private Context context;
    private MyOrders myOrders;

    public VerticalMyordersAdapt (Context context, MyOrders myOrders)

    {
        this.context= context;
        this.myOrders = myOrders;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.my_orders_vertical, viewGroup, false);
        viewHolder = new VerticalOrderViewHolder(view);
        return viewHolder;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof VerticalOrderViewHolder) {
        final VerticalOrderViewHolder vh= (VerticalOrderViewHolder)holder;
            OrderDetail orderDetail = myOrders.getOrderDetail().get(i);

        vh.tv_orderNo.setText("#"+" "+String.valueOf(orderDetail.getOrderId()));

       // vh.recyclerView.setVisibility(View.GONE);
      //  vh.linearLayout.setVisibility(View.GONE);
//        notifyDataSetChanged();

        OrderDetail_ orderDetailUnder = orderDetail.getOrderDetails().get(0);
        vh.tv_paymentType.setText("Cash On Delivery");
        if (orderDetailUnder.getApproved())
        {        vh.tv_status.setText("Approved");
                vh.tv_status.setTextColor(context.getResources().getColor(R.color.item_title));
        }
        if (orderDetailUnder.getDelivered())
        {        vh.tv_status.setText("Delivered");
                vh.tv_status.setTextColor(context.getResources().getColor(R.color.browse));
        }
        if (orderDetailUnder.getOutForDelivery())
        {
            {        vh.tv_status.setText("Out for Delivery");
                    vh.tv_status.setTextColor(Color.YELLOW);
            }
        }
        if (orderDetailUnder.getPendingApprovel())
        {
            {        vh.tv_status.setText("Pending Approvel");
                     vh.tv_status.setTextColor(Color.BLUE);
            }
        }
        else {
            {        vh.tv_status.setText("Pending");
                vh.tv_status.setTextColor(Color.DKGRAY);
            }
        }


        vh.tv_amount.setText(String.valueOf(orderDetailUnder.getTotal())+" "+"AED");
        vh.tv_delivery.setText(orderDetailUnder.getExpectedDeliveryDate());

        vh.tv_date.setText(orderDetailUnder.getOrderDate());
        HorizontalMyordersAdapt horizontalMyordersAdapt = new HorizontalMyordersAdapt(context,orderDetailUnder);
        vh.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
        vh.recyclerView.setAdapter(horizontalMyordersAdapt);
            vh.tv_hideDetails.setOnClickListener(new View.OnClickListener() {
               // String details =vh.tv_hideDetails.getText().toString();
                @Override
                public void onClick(View view) {
                    if (vh.tv_hideDetails.getText().toString().equalsIgnoreCase("Details"))
                    {
                        vh.tv_hideDetails.setText("Hide");
                        vh.recyclerView.setVisibility(View.VISIBLE);
                        vh.linearLayout.setVisibility(View.VISIBLE);
                        vh.view.setVisibility(View.GONE);
                        Log.d("hide clicked","hide");
                    }

                    else {
                        vh.tv_hideDetails.setText("Details");
                        vh.recyclerView.setVisibility(View.GONE);
                        vh.linearLayout.setVisibility(View.GONE);
                        vh.view.setVisibility(View.VISIBLE);
                        Log.d("details clicked","details");
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return myOrders.getOrderDetail().size();
    }

    public class VerticalOrderViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_orderNo, tv_hideDetails, tv_paymentType, tv_date, tv_delivery, tv_amount, tv_status;
        private RecyclerView recyclerView;
        private LinearLayout linearLayout;
        private View view;

        public VerticalOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_orderNo = itemView.findViewById(R.id.mov_tv_orderno);
            tv_hideDetails = itemView.findViewById(R.id.mov_tv_details_hide);
            tv_paymentType = itemView.findViewById(R.id.mov_tv_type);
            tv_date = itemView.findViewById(R.id.mov_tv_date);
            tv_delivery = itemView.findViewById(R.id.mov_tv_delivery);
            tv_amount = itemView.findViewById(R.id.mov_tv_amount);
            tv_status = itemView.findViewById(R.id.mov_tv_status);
            recyclerView = itemView.findViewById(R.id.mov_recyclerview);
            linearLayout = itemView.findViewById(R.id.mov_ll);
            view = itemView.findViewById(R.id.mov_view);

        }
    }
}
