package com.souq.uae.thebooksouq.Adapt;

import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Activity.CheckOutActivity;
import com.souq.uae.thebooksouq.Model.CartView.CartView;
import com.souq.uae.thebooksouq.Model.CartView.Datum;
import com.souq.uae.thebooksouq.Model.DeleteCartItem.DeleteItem;
import com.souq.uae.thebooksouq.Model.UpdateCart.UpdateCart;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartAdapt extends RecyclerView.Adapter<CartAdapt.CartAdaptViewHolder> {

    private int click = 0, click2 = 0, initialPrice;
    private Context context;
    private CartView cartViewList;
    private LayoutInflater layoutInflater;
    private Activity activity;
    private TextView textView;
    private Button button;
    private AlertDialog alertDialog;


    public CartAdapt(Context context, CartView cartViewList, TextView textView, Button button) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.cartViewList = cartViewList;
        this.textView = textView;
        this.button = button;
        activity = (Activity) context;

    }

    @NonNull
    @Override
    public CartAdaptViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = layoutInflater.inflate(R.layout.cart_row, viewGroup, false);
        CartAdaptViewHolder cartAdaptViewHolder = new CartAdaptViewHolder(view);

        return cartAdaptViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdaptViewHolder cartAdaptViewHolder, int i) {
        // cartAdaptViewHolder.imgv_cartRowCover.setImageResource(cartDetails.get(i).getImage());
        Picasso.get().load(Utils.HOME_BASE_URL_IMAGE + cartViewList.getCart().getData().get(i).getBookImage()).into(cartAdaptViewHolder.imgv_cartRowCover);
        cartAdaptViewHolder.tv_bookName.setText(cartViewList.getCart().getData().get(i).getBookName());
        cartAdaptViewHolder.tv_merchantName.setText(cartViewList.getCart().getData().get(i).getMerchantName());
        cartAdaptViewHolder.tv_price.setText(String.valueOf(cartViewList.getCart().getData().get(i).getQuantityPrice()) + " " + "AED");
        cartAdaptViewHolder.tv_noOfItems.setText(String.valueOf(cartViewList.getCart().getData().get(i).getBookQuantity()));

    }

    @Override
    public int getItemCount() {
        return cartViewList.getCart().getData().size();
    }

    public class CartAdaptViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgv_cartRowCover;
        ImageButton imgbtn_plus, imgbtn_minus, imgbtn_cartDelete;
        TextView tv_bookName, tv_merchantName, tv_price/*, tv_plus, tv_minus*/, tv_noOfItems;

        public CartAdaptViewHolder(@NonNull View itemView) {
            super(itemView);

            imgv_cartRowCover = itemView.findViewById(R.id.imgv_cart_row_cover);
            tv_bookName = itemView.findViewById(R.id.tv_cartrow_bookName);
            tv_merchantName = itemView.findViewById(R.id.tv_cartrow_merchant);
            tv_price = itemView.findViewById(R.id.tv_cartrow_price);
            imgbtn_cartDelete = itemView.findViewById(R.id.imgbtn_cartrow_delete);
            imgbtn_plus = itemView.findViewById(R.id.imgbtn_cartrow_plus);
            imgbtn_minus = itemView.findViewById(R.id.imgbtn_cartrow_minus);
            tv_noOfItems = itemView.findViewById(R.id.tv_cartrow_noOfItems);


            imgbtn_plus.setTag(R.integer.btn_plus_view, itemView);
            imgbtn_minus.setTag(R.integer.btn_minus_view, itemView);

            imgbtn_cartDelete.setOnClickListener(this);
            imgbtn_plus.setOnClickListener(this);
            imgbtn_minus.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.imgbtn_cartrow_delete:


                    deleteRequest(getAdapterPosition());
                    // removeAt(getAdapterPosition());
                    break;
                case R.id.imgbtn_cartrow_plus:
                    activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    //            Utils.setProgressDialog(context);
                    String bookId = cartViewList.getCart().getData().get(getAdapterPosition()).getBookId().toString();

                    updateCartRequest(bookId, "1", "0", getAdapterPosition());


          /*  click++;
            View tempView = (View) tv_plus.getTag(R.integer.btn_plus_view);
            TextView textView = (TextView) tempView.findViewById(R.id.tv_cartrow_noOfItems);
            TextView textView1 = (TextView) tempView.findViewById(R.id.tv_cartrow_price);
            int noOfInitialItems = Integer.parseInt(textView.getText().toString());
            if (click==1) { initialPrice = Integer.parseInt(textView1.getText().toString()) / noOfInitialItems; }
            int noOfItems = Integer.parseInt(textView.getText().toString())+1;
            textView.setText(String.valueOf(noOfItems));
            cartDetails.get(getAdapterPosition()).setCount(String.valueOf(noOfItems));
            int afterPrice = initialPrice*noOfItems;
            textView1.setText(String.valueOf(afterPrice));
             cartDetails.get(getAdapterPosition()).setPrice(String.valueOf(afterPrice));*/
                    break;
                case R.id.imgbtn_cartrow_minus:
                    if (cartViewList.getCart().getData().get(getAdapterPosition()).getBookQuantity() > 1) {
                        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        //              Utils.setProgressDialog(context);
                        String bookIde = cartViewList.getCart().getData().get(getAdapterPosition()).getBookId().toString();
                        //Utils.setProgressDialog(context);
                        updateCartRequest(bookIde, "0", "1", getAdapterPosition());
                    }
            /* click2++;
            View temp_View = (View) tv_minus.getTag(R.integer.btn_minus_view);
            TextView tv = (TextView) temp_View.findViewById(R.id.tv_cartrow_noOfItems);
            TextView tv2= (TextView) temp_View.findViewById(R.id.tv_cartrow_price);
            if (!tv.getText().toString().equals("0")) {
                int no_ofInitialItems = Integer.parseInt(tv.getText().toString());
                if (click2 == 1) {
                    initialPrice = Integer.parseInt(tv2.getText().toString()) / no_ofInitialItems;
                }
                int no_ofItems = Integer.parseInt(tv.getText().toString()) - 1;

                tv.setText(String.valueOf(no_ofItems));
                cartDetails.get(getAdapterPosition()).setCount(String.valueOf(no_ofItems));


                int price_minus = Integer.parseInt(tv2.getText().toString()) - initialPrice;
                tv2.setText(String.valueOf(price_minus));
                cartDetails.get(getAdapterPosition()).setPrice(String.valueOf(price_minus));
            }*/

                    break;
            }


        }
    }

    public void removeAt(int position) {

        // Activity activity = (Activity) context;
        // activity.invalidateOptionsMenu();
        cartViewList.getCart().getData().remove(position);
        //notifyItemRemoved(position);
        //notifyItemRangeChanged(position, cartViewList.getCart().getData().size());
        notifyDataSetChanged();

    }

    public void updateAt(int position, Double price, Integer quantity) {
        cartViewList.getCart().getData().get(position).setQuantityPrice(price);
        cartViewList.getCart().getData().get(position).setBookQuantity(quantity);

        notifyDataSetChanged();
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        //Utils.disappearProgressDialog();
    }

    private void deleteRequest(final Integer position) {
        //Utils.setProgressDialog(context);
        //prgressDialogShow();
        if (Utils.isInternetAvailable(context)) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            Map<String, String> data = new HashMap<>();
            data.put("cart_id", Utils.getCartId(context));
            data.put("book_id", String.valueOf(cartViewList.getCart().getData().get(position).getBookId()));
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().deleteCartItem(data).enqueue(new Callback<DeleteItem>() {
                @Override
                public void onResponse(Call<DeleteItem> call, Response<DeleteItem> response) {
                    if (response.body().getMsg().equalsIgnoreCase("Delete Book Successfully")) {
                        if (response.body().getStatus() == 200) {
                            if (response.body().getTotalPrice() != null) {
                                textView.setText(response.body().getTotalPrice().toString() + " " + "AED");
                                button.setBackgroundResource(R.drawable.button_selector);
                            } else if (response.body().getTotalPrice() == null) {
                                textView.setText("0");

                            }
                            if (response.body().getCart_quantity() == null) {
                                Utils.saveCartCount(context, 0);
                                button.setBackgroundResource(R.drawable.button_selector_quantity);
                            } else if (response.body().getCart_quantity() != null) {
                                Utils.saveCartCount(context, Integer.valueOf(response.body().getCart_quantity()));
                            }

                            activity.invalidateOptionsMenu();
                            removeAt(position);
                            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                         //   Utils.disappearProgressDialog();
                            //progressDialogDismis();

                        }
                    }

                    //else {
//                        progressDialogDismis();
               //         Utils.disappearProgressDialog();
                    //}
//                    progressDialogDismis();
                 //   Utils.disappearProgressDialog();
                }

                @Override
                public void onFailure(Call<DeleteItem> call, Throwable t) {
                    t.printStackTrace();
//                    progressDialogDismis();
             //       Utils.disappearProgressDialog();
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });
           // Utils.disappearProgressDialog();
            //            progressDialogDismis();
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
         //   Utils.disappearProgressDialog();
            //progressDialogDismis();
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    deleteRequest(position);
                }
            });
            dialog.show();
        }
       // Utils.disappearProgressDialog();
        //progressDialogDismis();
    }

    private void updateCartRequest(final String bookId, final String add, final String remove, final Integer position) {

         //       Utils.setProgressDialog(context);
        if (Utils.isInternetAvailable(context)) {
            Map<String, String> data = new HashMap<>();
            data.put("book_id", bookId);
            data.put("cart_id", Utils.getCartId(context));
            data.put("add", add);
            data.put("remove", remove);
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().updateCartItem(data).enqueue(new Callback<UpdateCart>() {
                @Override
                public void onResponse(Call<UpdateCart> call, Response<UpdateCart> response) {
                    if (response.body() != null) {
                        Utils.saveCartCount(context, Integer.valueOf(response.body().getCart_quantity()));
                        activity.invalidateOptionsMenu();
                        updateAt(position, response.body().getQuantityPrice(), response.body().getItemQuantity());
                        textView.setText(response.body().getTotalPrice() + " " + "AED");
                        // Utils.disappearProgressDialog();
                        if (response.body().getMsg().equalsIgnoreCase("This book is out of stock")) {
                            // Utils.disappearProgressDialog();
                            Utils.alertDialog(context, response.body().getMsg());
                        }
                        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        //Utils.disappearProgressDialog();
                    } else {
           //             Utils.disappearProgressDialog();
                        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
             //       Utils.disappearProgressDialog();
                }

                @Override
                public void onFailure(Call<UpdateCart> call, Throwable t) {
                    t.printStackTrace();
               //     Utils.disappearProgressDialog();
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });
           // Utils.disappearProgressDialog();
        } else {
        //    Utils.disappearProgressDialog();
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    updateCartRequest(bookId, add, remove, position);
                }
            });
            dialog.show();
        }
        //Utils.disappearProgressDialog();
    }

    private void prgressDialogShow() {

        alertDialog = new AlertDialog.Builder(context).create();
        View progressbarLayout = layoutInflater.inflate(R.layout.progressbar, null);
        ProgressBar progressBar = progressbarLayout.findViewById(R.id.progressBar_seprate);
        alertDialog.setView(progressbarLayout);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    private void progressDialogDismis() {
        alertDialog.dismiss();
    }
}
