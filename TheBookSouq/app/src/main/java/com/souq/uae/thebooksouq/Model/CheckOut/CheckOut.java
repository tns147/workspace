package com.souq.uae.thebooksouq.Model.CheckOut;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckOut {

    @SerializedName("status")
    @Expose
    private String status;
/*
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("cart_id")
    @Expose
    private String cartId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
*/
    @SerializedName("token")
    @Expose
    private String token;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

/*
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
*/

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}