package com.souq.uae.thebooksouq.Network;

import com.souq.uae.thebooksouq.Model.AddToCart.AddToCart;
import com.souq.uae.thebooksouq.Model.BookDescription.BookDescription;
import com.souq.uae.thebooksouq.Model.CartView.CartView;
import com.souq.uae.thebooksouq.Model.CheckOut.CheckOut;
import com.souq.uae.thebooksouq.Model.DeleteCartItem.DeleteItem;
import com.souq.uae.thebooksouq.Model.GeoModels.MainObject;
import com.souq.uae.thebooksouq.Model.HereGeocode.ReverseGeoCoding;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.MainBooks;
import com.souq.uae.thebooksouq.Model.LocationIQ.LocationIqReverseGeocoding;
import com.souq.uae.thebooksouq.Model.Login.Login;
import com.souq.uae.thebooksouq.Model.MobileEmailValidaiton.PhoneEmailValidation;
import com.souq.uae.thebooksouq.Model.MyOrders.MyOrders;
import com.souq.uae.thebooksouq.Model.OrderComplete.OrderComplete;
import com.souq.uae.thebooksouq.Model.OrderProceed.OrderProcced;
import com.souq.uae.thebooksouq.Model.ReviewModel.Review;
import com.souq.uae.thebooksouq.Model.SearchResults.SearchResults;
import com.souq.uae.thebooksouq.Model.SeeAll.SeeAllMain;
import com.souq.uae.thebooksouq.Model.SignUp.SignUp;
import com.souq.uae.thebooksouq.Model.SuggestionModel.SuggestUpdated;
import com.souq.uae.thebooksouq.Model.TwillioVerification.SendSms;
import com.souq.uae.thebooksouq.Model.TwillioVerification.VerifySms;
import com.souq.uae.thebooksouq.Model.UpdateCart.UpdateCart;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface WebRequestGeo {
    @GET("api/geocode/json") // specify the sub url for our base url
    //public void getVideoList(Callback<List<CountryResponse>> callback);
     Call<MainObject> getLocationDetails(
            @Query("latlng") String latLong,
            @Query("key") String key);


   /* @GET("/api/books-search1")
    Call<MainBooks>getHomedata(@Query("start") int start, @Query("end") int end);*/

/*
    @GET("/api/consumer/books-browse")
    Call<MainBooks>getHomedata(
            @Query("page") String pageNo);
*/


    @GET("/api/consumer/books-browse-home-screen")
    Call<MainBooks>getHomedata(
            @Query("page") String pageNo);


/*
    @GET("/api/books-search")
    Call<SearchActiva>getCompleSearch(
            @Query("q") String text);
*/

    @GET("/api/consumer/auto-suggest")
    Call<SuggestUpdated>getSearchSuggestions(
            @Query("suggest") String text);

    @GET("api/consumer/category-books/{category}")
    Call<SeeAllMain>getCategoryItems(
            @Path(value="category") String category,
            @Query("page")String text);
    @GET("api/consumer/search-by-filters")
    Call<SearchResults>getBrowseSearchResults(
            @QueryMap Map<String,String>options);

    @GET("api/consumer/category-books/{category}")
    Call<SeeAllMain>getCategorySearch(
            @Path(value="category") String category,
            @Query("q")String text,
            @Query("page")String page);

    @GET("api/consumer/book-detail/{id}")
    Call<BookDescription>getBookDetails(
            @Path(value="id") String id,
            @Query("cart_id") String cartId);

    @FormUrlEncoded
    @POST("api/consumer/add-to-cart")
    Call<AddToCart>addToCart(@FieldMap Map<String,String>details);

    @GET("api/consumer/view-cart/{cartid}")
    Call<CartView>getCartView(
            @Path(value="cartid") String cartId);
    @HTTP(method = "DELETE",path ="api/consumer/remove-item-in-cart/",hasBody = true)
    Call<DeleteItem> deleteCartItem(@Body Map<String,String> body);

    @HTTP(method = "PUT",path = "api/consumer/update-item-quantity-in-cart/",hasBody = true)
    Call<UpdateCart>updateCartItem(@Body Map<String,String>body);

    @FormUrlEncoded
    @POST("api/consumer/sign-up/")
    Call<SignUp>signUp(@FieldMap Map<String,String>details);

    @FormUrlEncoded
    @POST("api/consumer/login/")
    Call<Login>login(@FieldMap Map<String,String>details);

    @FormUrlEncoded
    @POST("api/consumer/check-out/")
    Call<CheckOut>checkOutCheck(@Header("Authorization")String token, @FieldMap Map<String,String> data);

    @FormUrlEncoded
    @POST("api/consumer/order-review/")
    Call<Review>orderReview(@Header("Authorization")String token, @FieldMap Map<String,String> data);

/*
        @POST("/verify/token")
        @FormUrlEncoded
        Call<TokenServerResponse> getToken(@Field("phone_number") String phoneNumber);
*/

    @FormUrlEncoded
    @POST("api/consumer/order-proceed/")
    Call<OrderProcced>orderProceed(@Header("Authorization")String token, @FieldMap Map<String,String>data);

  /*  @FormUrlEncoded
    @POST("api/consumer/order-complete/")
    Call<OrderComplete>orderComplete(@Header("Authorization")String token, @FieldMap Map<String,String>data);
*/
    @FormUrlEncoded
    @POST("api/consumer/order-complete-by-cash-on-delivery/")
    Call<OrderComplete>orderComplete(@Header("Authorization")String token, @FieldMap Map<String,String>data);


    @FormUrlEncoded
    @POST("protected/json/phones/verification/start")
    Call<SendSms>sendSms(@Header("X-Authy-API-Key")String apiKey, @FieldMap Map<String,String>data);

    @GET("protected/json/phones/verification/check")
    Call<VerifySms>verifySms(@Header("X-Authy-API-Key")String apiKey,@QueryMap Map<String,String >data);

    @GET("6.2/reversegeocode.json")
    Call<ReverseGeoCoding> hereReverseGeoCoding(@QueryMap Map<String,String>data);

    @GET("v1/reverse.php")
    Call<LocationIqReverseGeocoding>locationIqReverseGeocoding(@QueryMap Map<String,String >data);

    @GET("api/consumer/check-phone-number-and-email-exists")
    Call<PhoneEmailValidation>phoneEmailValidation(@QueryMap Map<String,String>data);

    @GET("api/consumer/get-order-details-by-id/{id}")
    Call<MyOrders> myOrders(@Path(value = "id") String userId);
}
