package com.souq.uae.thebooksouq.Fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Activity.CheckOutActivity;
import com.souq.uae.thebooksouq.Activity.HomeActivity;
import com.souq.uae.thebooksouq.Activity.MobileNoActivity;
import com.souq.uae.thebooksouq.Activity.OrdersReviewActivity;
import com.souq.uae.thebooksouq.Activity.RegistrationActivity;
import com.souq.uae.thebooksouq.Adapt.CartAdapt;
import com.souq.uae.thebooksouq.Model.CartView.CartView;
import com.souq.uae.thebooksouq.Model.CheckOut.CheckOut;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {
    private Context context;
    private TextView tv_price;
    private Button button_update, button_checkOut;
    private RecyclerView recyclerView_cartDetails;
    private String TAG = CartFragment.class.getSimpleName();
    private LinearLayoutManager layoutManager;
    private RelativeLayout relativeLayout;
    private ProgressBar progressBar;


    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "entered in createView");

        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        // this.container = container.getContext();
        context = container.getContext();
        findViews(view);


        button_checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "entered in onclick");
                if (Utils.getCartCount(context) > 0 && Utils.getToken(context).equalsIgnoreCase("0")) {
                    Intent intent = new Intent(context, RegistrationActivity.class);
                    startActivity(intent);
                } else if (Utils.getCartCount(context) > 0 && !(Utils.getToken(context).equalsIgnoreCase("0"))) {
                    Utils.setProgressDialog(context);
                    checkOutCheck(Utils.getToken(context), Utils.getUserId(context), Utils.getCartId(context));

                }


            }
        });
        return view;
    }

    @Override
    public void onResume() {
        cartView();
        super.onResume();
    }

    private void cartView() {
        Log.d(TAG, "entered in cartview");
        if (Utils.isInternetAvailable(context)) {
            relativeLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            Log.d(TAG, "entered in cartview internet available");
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getCartView(Utils.getCartId(context)).enqueue(new Callback<CartView>() {
                @Override
                public void onResponse(Call<CartView> call, Response<CartView> response) {

                    if (response.body() != null) {
                        Log.d(TAG, "entered in responsebody!=null");
                        if (response.body().getCart().getData().size() > 0) {
                            Utils.saveCartCount(context, response.body().getCart().getCartQuantity().get(0).getQuantity());
                            getActivity().invalidateOptionsMenu();
                            tv_price.setText(String.valueOf(response.body().getCart().getTotal().get(0).getTotalPrice()) + " " + "AED");
                            Log.d(TAG, "entered in cartsize>0");
                            CartAdapt cartAdapt = new CartAdapt(context, response.body(), tv_price, button_checkOut);
                            recyclerView_cartDetails.setAdapter(cartAdapt);
                            relativeLayout.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG, "entered in cartsize<0");
                            Utils.saveCartCount(context, 0);
                            getActivity().invalidateOptionsMenu();
                            tv_price.setText("0 AED");
                            recyclerView_cartDetails.setAdapter(null);
                            button_checkOut.setBackgroundResource(R.drawable.button_selector_quantity);
                            button_checkOut.setTextColor(getResources().getColor(R.color.quantity_text));
                            relativeLayout.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);


                        }
                    } else {
                        Log.d(TAG, "responsebody==null");
                        tv_price.setText("0 AED");
                        recyclerView_cartDetails.setAdapter(null);
                        relativeLayout.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<CartView> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    cartView();
                }
            });
            dialog.show();
        }
    }

    private void findViews(View view) {
        recyclerView_cartDetails = view.findViewById(R.id.cartDetails_recyclerView);
        tv_price = view.findViewById(R.id.fcart_price);
        //button_update = view.findViewById(R.id.fCart_btn_update);
        button_checkOut = view.findViewById(R.id.fCart_btn_checkout);
        relativeLayout = view.findViewById(R.id.fc_rl_main);
        progressBar = view.findViewById(R.id.fc_progressbar);
        layoutManager = (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView_cartDetails.setHasFixedSize(true);
        recyclerView_cartDetails.setItemViewCacheSize(2);
        recyclerView_cartDetails.setDrawingCacheEnabled(true);
        recyclerView_cartDetails.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView_cartDetails.setLayoutManager(layoutManager);

    }

    private void checkOutCheck(final String token, final String userId, final String cartId)

    {

        if (Utils.isInternetAvailable(context)) {
            Map<String, String> data = new HashMap<>();
            data.put("user_id", userId);
            data.put("cart_id", cartId);

            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().checkOutCheck(token, data).enqueue(new Callback<CheckOut>() {
                @Override
                public void onResponse(Call<CheckOut> call, Response<CheckOut> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("ok")) {
                            Utils.saveToken(context, response.body().getToken());
                            Intent intent = new Intent(context, OrdersReviewActivity.class);
                            startActivity(intent);
                          //  Toast.makeText(context, Utils.getToken(context), Toast.LENGTH_SHORT).show();

                        } else if (response.body().getStatus().equalsIgnoreCase("User not register")) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                            dialog.setMessage(R.string.login_again);
                            dialog.setCancelable(false);
                            dialog.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    Utils.updateUserId(context, "-1");
                                    Utils.updateCartId(context, "0");
                                    Utils.saveToken(context, "0");
                                    Utils.saveCartCount(context, 0);
                                    Intent intent = new Intent(context, HomeActivity.class);
                                    startActivity(intent);
                                   // Toast.makeText(context, Utils.getToken(context), Toast.LENGTH_SHORT).show();
                                }
                            });
                            dialog.show();
                        }

                    }
                    Utils.disappearProgressDialog();
                }

                @Override
                public void onFailure(Call<CheckOut> call, Throwable t) {
                    t.printStackTrace();
                    Utils.disappearProgressDialog();
                }
            });
        } else {
            Utils.disappearProgressDialog();
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkOutCheck(token, userId, cartId);
                }
            });
            dialog.show();
        }
    }

}
