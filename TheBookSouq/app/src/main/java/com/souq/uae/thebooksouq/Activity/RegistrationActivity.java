package com.souq.uae.thebooksouq.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.souq.uae.thebooksouq.Model.GeoModels.MainObject;
import com.souq.uae.thebooksouq.Model.HereGeocode.ReverseGeoCoding;
import com.souq.uae.thebooksouq.Model.LocationIQ.LocationIqReverseGeocoding;
import com.souq.uae.thebooksouq.Model.MobileEmailValidaiton.PhoneEmailValidation;
import com.souq.uae.thebooksouq.Model.SignUp.SignUp;
import com.souq.uae.thebooksouq.Model.TwillioVerification.SendSms;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //private TwilioVerification twilioVerification;
    private AwesomeValidation mAwesomeValidation;
    private GoogleApiClient mLocationClient;
    private LocationRequest mLocationRequest = new LocationRequest();
    //protected Handler handler;
    private String longitude, latitude;
    private final String TAG = RegistrationActivity.class.getSimpleName();
    private EditText et_name, et_phoneNo, et_email, et_address;
    private Button btn_next;
    private Toolbar toolbar;
    private Map<String, String> data = new HashMap<>();
    private boolean check = true;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private TextView tv_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        findViews();

    }

    @Override
    protected void onResume() {
        //mLocationClient.disconnect();
        mLocationClient = null;
        check = true;
        showSetting();
        super.onResume();
    }


    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed");
        mLocationClient.disconnect();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();
        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void findViews() {
        tv_login = findViewById(R.id.registration_tv_login);
        et_name = findViewById(R.id.registration_et_full_name);
        et_phoneNo = findViewById(R.id.registration_et_phoneno);
        //et_phoneNo.setText(getIntent().getStringExtra("mobile"));
        //et_phoneNo.setEnabled(false);
        et_address = findViewById(R.id.registration_et_location);
        et_email = findViewById(R.id.registration_et_email);
        btn_next = findViewById(R.id.registration_btn_next);
        toolbar = findViewById(R.id.toolbar_activity_registration);
        toolbar.setTitle(" ");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = et_email.getText().toString().trim();
                String phoneNo = et_phoneNo.getText().toString().trim();
                String name = et_name.getText().toString().trim();
                String address = et_address.getText().toString();
                mAwesomeValidation = new AwesomeValidation(ValidationStyle.COLORATION);
                if (!(email.matches(emailPattern))) {
                    mAwesomeValidation.addValidation(et_email, emailPattern, getResources().getString(R.string.email_validity_msg));

                }

                if (address.length() == 0) {

                    mAwesomeValidation.addValidation(et_address, RegexTemplate.NOT_EMPTY, getResources().getString(R.string.address_validity_msg));
                }
                if (address.length() < 10 && address.length() != 0) {
                    et_address.getText().clear();
                    et_address.setHint(getResources().getString(R.string.location_hint));
                }

                if (phoneNo.length() != 9) {
                    Log.d(TAG, String.valueOf(phoneNo.length()));

                    mAwesomeValidation.addValidation(et_phoneNo, "[0-9]{9}", getResources().getString(R.string.phoneno_validity_msg));
                }
                if (name.length() == 0) {
                    mAwesomeValidation.addValidation(et_name, RegexTemplate.NOT_EMPTY, getResources().getString(R.string.name_validity_msg));
                }
                if (name.length() != 0 && name.length() < 5) {
                    et_name.getText().clear();
                    et_name.setHint(getResources().getString(R.string.hint_fullname));
                }

                mAwesomeValidation.validate();

                if (email.matches(emailPattern) && /*phoneNo.length() == 10 &&*/ name.length() > 4 && address.length() > 9 &&phoneNo.matches("[0-9]{9}")) {
                    mLocationClient.disconnect();
                    Utils.setProgressDialog(RegistrationActivity.this);
                    //                  siginingUp();
                    // twillioSendSms(et_phoneNo.getText().toString());
                    //dataTransfer();
                    mobileEmailValidation(phoneNo,email);

                }

            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }


    /*private void geoCodeAddress(String latitude, String longitude) {

        LoginRetrofitClass.getGeoInstance().getWebRequestsInstance().getLocationDetails(latitude + "," + longitude, Utils.API_KEY_GEOCODE).enqueue(new Callback<MainObject>() {
            @Override
            public void onResponse(Call<MainObject> call, Response<MainObject> response) {
                Log.d("Homefragment", "Responded");

                if (response.body() != null) {
                    Toast.makeText(RegistrationActivity.this, "Responded", Toast.LENGTH_SHORT).show();
                    Log.d("Homefragment", "geocodebody not null");

                    if (!(response.body().getStatus().equals("OVER_QUERY_LIMIT"))) {
                        String addr = response.body().getResults().get(0).getFormattedAddress();
                        Utils.updateLocation(RegistrationActivity.this, addr);
                        et_address.setText(Utils.getLocation(RegistrationActivity.this));
                    }
                }

            }

            @Override
            public void onFailure(Call<MainObject> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(RegistrationActivity.this, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
            }
        });
    }*/
    private void geoCodeAddress(final String latitude, final String longitude) {
        if (Utils.isInternetAvailable(RegistrationActivity.this)) {
            Map<String, String> data = new HashMap<>();
            data.put("key", Utils.LOCATION_IQ_KEY);
            data.put("lat", latitude);
            data.put("lon", longitude);
            data.put("format","json");


            LoginRetrofitClass.getGeoInstance().getWebRequestsInstance().locationIqReverseGeocoding(data).enqueue(new Callback<LocationIqReverseGeocoding>() {
                @Override
                public void onResponse(Call<LocationIqReverseGeocoding> call, Response<LocationIqReverseGeocoding> response) {
                    Log.d("Homefragment", "Responded");


                    if (response.body() != null) {

                      //  Toast.makeText(RegistrationActivity.this, "Responded", Toast.LENGTH_SHORT).show();
                        Log.d("HomeActivity", "geocodebody not null");

                        if (response.body().getDisplayName() != null) {
                            String addr = response.body().getDisplayName();
                            Utils.updateLocation(RegistrationActivity.this, addr);
                            et_address.setText(Utils.getLocation(RegistrationActivity.this));

                        } else {
                         //   Toast.makeText(RegistrationActivity.this, "NO Location", Toast.LENGTH_SHORT).show();
                        }
                    }
                }


                @Override
                public void onFailure(Call<LocationIqReverseGeocoding> call, Throwable t) {
                    t.printStackTrace();
                    //Toast.makeText(RegistrationActivity.this, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(RegistrationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                   geoCodeAddress(latitude, longitude);
                }
            });
            dialog.show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Error On Connection Failed!");
       // Toast.makeText(this, "No Internet Connectivity Address failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            final String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions((Activity) this, permissions, 8);

            //getLocation();
            Log.d(TAG, "Error On Connection Permission Not Granted!");
            //Toast.makeText(this, "Please Allow Access to Location", Toast.LENGTH_SHORT).show();

            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        Log.d(TAG, "Connected to Google API");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location changed");

        if (location != null) {
            Log.d(TAG, "== location != null");
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            if (check) {
                geoCodeAddress(latitude, longitude);
                check = false;
            }
        }
    }

    private void getLocation() {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);

        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;

        mLocationRequest.setPriority(priority);
        mLocationClient.connect();
    }

    private void showSetting() {
        if (mLocationClient == null) {
            mLocationClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mLocationClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mLocationClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            getLocation();
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        RegistrationActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        showSetting();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }




    private void dataTransfer() {
        Intent intent = new Intent(RegistrationActivity.this, ValidationActivity.class);
        intent.putExtra("full_name", et_name.getText().toString());
        intent.putExtra("email", et_email.getText().toString());
        intent.putExtra("address", et_address.getText().toString());
        intent.putExtra("mobile", et_phoneNo.getText().toString());
        intent.putExtra("activity","registration");
        startActivity(intent);
    }

    private void twillioSendSms(final String phoneNo) {
        Map<String, String> data = new HashMap<>();
        data.put("via", "sms");
        data.put("phone_number", phoneNo);
        data.put("country_code", Utils.COUNTRY_CODE);
        if (Utils.isInternetAvailable(RegistrationActivity.this)) {

            LoginRetrofitClass.getTwilioInstance().getTwillioService().sendSms(Utils.TWILLIO_API_KEY, data).enqueue(new Callback<SendSms>() {
                @Override
                public void onResponse(Call<SendSms> call, Response<SendSms> response) {
                    if (response.body() != null) {
                        if (response.body().getSuccess()) {
                            Utils.disappearProgressDialog();
                            dataTransfer();

                        } else {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(RegistrationActivity.this, response.body().getMessage());
                        }
                    } else {
                        Utils.disappearProgressDialog();
                        Utils.alertDialog(RegistrationActivity.this, getString(R.string.invalid_no));
                    }
                }

                @Override
                public void onFailure(Call<SendSms> call, Throwable t) {
                    Utils.disappearProgressDialog();
                    t.printStackTrace();
                }
            });
        } else {
            Utils.disappearProgressDialog();
            AlertDialog.Builder dialog = new AlertDialog.Builder(RegistrationActivity.this);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    twillioSendSms(phoneNo);
                }
            });
            dialog.show();
        }
    }


private void mobileEmailValidation(final String mobile, final String email)
{
    if (Utils.isInternetAvailable(this))
    {
        final Map<String,String >data = new HashMap<>();
        data.put("mobile",mobile);
        data.put("email",email);
        LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().phoneEmailValidation(data).enqueue(new Callback<PhoneEmailValidation>() {
            @Override
            public void onResponse(Call<PhoneEmailValidation> call, Response<PhoneEmailValidation> response) {
                if (response.body()!=null)
                {
                    if (response.body().getStatus().equalsIgnoreCase("ok"))
                    {
                        if (response.body().getInfo()!=null)
                        {
                        String email = response.body().getInfo().getEmail();
                        String phoneNo = response.body().getInfo().getPhoneNumber();
                        if (email.equalsIgnoreCase("no")&&phoneNo.equalsIgnoreCase("no"))
                        {
                            twillioSendSms(mobile);
                           // dataTransfer();
                        }
                        else if (email.equalsIgnoreCase("yes")&&phoneNo.equalsIgnoreCase("yes"))
                        {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(RegistrationActivity.this,getString(R.string.phone_email_exsist));
                        }

                        else if (email.equalsIgnoreCase("no")&&phoneNo.equalsIgnoreCase("yes"))
                        {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(RegistrationActivity.this,getString(R.string.phone_already_exsist));
                        }
                        else if (email.equalsIgnoreCase("yes")&&phoneNo.equalsIgnoreCase("no"))
                        {
                            Utils.disappearProgressDialog();
                            Utils.alertDialog(RegistrationActivity.this,getString(R.string.email_already_exsist));
                        }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<PhoneEmailValidation> call, Throwable t) {
                Utils.disappearProgressDialog();
                t.printStackTrace();
            }
        });
    }
    else {
        Utils.disappearProgressDialog();

        AlertDialog.Builder dialog = new AlertDialog.Builder(RegistrationActivity.this);
        dialog.setMessage(R.string.internet_problem);
        dialog.setCancelable(false);
        dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mobileEmailValidation(mobile, email);
            }
        });
        dialog.show();
    }
}
}
