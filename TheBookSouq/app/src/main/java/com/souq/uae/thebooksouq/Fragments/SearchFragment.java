package com.souq.uae.thebooksouq.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Activity.SearchrResultsActivity;
import com.souq.uae.thebooksouq.Adapt.SearchSuggestionsAdapter;
import com.souq.uae.thebooksouq.Activity.HomeActivity;
import com.souq.uae.thebooksouq.Model.SuggestionModel.SuggestUpdated;
import com.souq.uae.thebooksouq.Network.LoginRetrofitClass;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private SearchView searchView;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private List<String> datumList;
    private SearchSuggestionsAdapter searchSuggestionsAdapter;
    private CheckBox cb_author, cb_bookTitle, cb_merchant, cb_genre;
    private String st_author="false", st_bookTitle="false", st_merchant="false", st_genre="false";
    private int int_author, int_bookTitle, int_merchant, int_genre;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        context = container.getContext();
        findViews(view);
        return view;
    }

    private void findViews(View view) {
        searchView = view.findViewById(R.id.frag_search_sv);
        searchView.setIconified(false);
        searchView.setQueryHint("Search");
        View v = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        v.setBackgroundColor(Color.TRANSPARENT);
        recyclerView = view.findViewById(R.id.frag_search_rv);
        layoutManager = (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(2);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setLayoutManager(layoutManager);

        cb_author = view.findViewById(R.id.frag_search_checkbox_author);
        cb_bookTitle = view.findViewById(R.id.frag_search_checkbox_bookTitle);
        cb_genre = view.findViewById(R.id.frag_search_checkbox_genre);
        cb_merchant = view.findViewById(R.id.frag_search_checkbox_merchant);


        cb_author.setOnClickListener(this);
        cb_bookTitle.setOnClickListener(this);
        cb_genre.setOnClickListener(this);
        cb_merchant.setOnClickListener(this);

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
               // getFragmentManager().popBackStack();
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                if (!(s == null || s.equals("") || s.equals(" ") || s.length() ==0)) {
                   /* SearchResultsFragment searchResultsFragment = new SearchResultsFragment();
                    Bundle args = new Bundle();
                    args.putString(Utils.SEARCH_SUBMIT_KEY, s);
                    searchResultsFragment.setArguments(args);
                    HomeActivity homeActivity = (HomeActivity) context;
                    FragmentTransaction fragmentTransaction = homeActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_home, searchResultsFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();*/
                    Intent intent =  new Intent(context, SearchrResultsActivity.class);
                    intent.putExtra(Utils.SEARCH_SUBMIT_KEY,s);
                    context.startActivity(intent);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (!(s == null || s.equals("") || s.equals(" ") || s.length() <= 2)) {
                    searchSuggest(s);
                } else {
                    recyclerView.setAdapter(null);
                }
              /*
                else {datumList.clear();
                  //searchSuggestionsAdapter.notifyDataSetChanged();
                    recyclerView.removeAllViews();
                    recyclerView.removeAllViewsInLayout();
              }
*/

                return true;
            }
        });

    }

    private void searchSuggest(final String text) {
        if (Utils.isInternetAvailable(context)) {
            LoginRetrofitClass.getHomeInstance().getWebRequestsInstance2().getSearchSuggestions(text).enqueue(new Callback<SuggestUpdated>() {
                @Override
                public void onResponse(Call<SuggestUpdated> call, Response<SuggestUpdated> response) {

                    if (response.body() == null) {
                        datumList.clear();
                        searchSuggestionsAdapter = new SearchSuggestionsAdapter(datumList, context);
                        recyclerView.setAdapter(searchSuggestionsAdapter);
                        searchSuggestionsAdapter.notifyDataSetChanged();
                    }
                    if (response.body() != null) {
                        datumList = response.body().getSuggest();
                        searchSuggestionsAdapter = new SearchSuggestionsAdapter(datumList, context);
                        recyclerView.setAdapter(searchSuggestionsAdapter);
                        searchSuggestionsAdapter.notifyDataSetChanged();

                    }
                }

                @Override
                public void onFailure(Call<SuggestUpdated> call, Throwable t) {

                }
            });
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(R.string.internet_problem);
            dialog.setCancelable(false);
            dialog.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    searchSuggest(text);
                }
            });
            dialog.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.frag_search_checkbox_author:
                CheckBox checkBox_a = (CheckBox) view;
                if (checkBox_a.isChecked()) {
                    st_author = "true" ;
                  //  Toast.makeText(context, st_author, Toast.LENGTH_SHORT).show();
                } else {
                    st_author = "false" ;
                    //Toast.makeText(context, st_author, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.frag_search_checkbox_bookTitle:
                CheckBox checkBox_b = (CheckBox) view;
                if (checkBox_b.isChecked()) {
                    st_bookTitle = "true" ;
                } else {
                    st_bookTitle = "false" ;
                }


                break;

            case R.id.frag_search_checkbox_genre:
                CheckBox checkBox_g = (CheckBox) view;
                if (checkBox_g.isChecked()) {
                    st_genre = "true" ;
                } else {
                    st_genre = "false" ;
                }
                break;

            case R.id.frag_search_checkbox_merchant:
                CheckBox checkBox_m = (CheckBox) view;
                if (checkBox_m.isChecked()) {
                    st_merchant = "true" ;
                } else {
                    st_merchant = "false" ;
                }

                break;
        }
    }
}
