package com.souq.uae.thebooksouq.Adapt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.souq.uae.thebooksouq.Activity.SeeAllBooksActivity;
import com.souq.uae.thebooksouq.Fragments.SeeAllBooksFragment;
import com.souq.uae.thebooksouq.Activity.HomeActivity;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.Book;
import com.souq.uae.thebooksouq.Model.HomeFragmentModel.Datum;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;

import java.util.List;


public class VerticalAdapt extends RecyclerView.Adapter {


    //private MainBooks mainBooksModel;
    private List<Book> booksList;
    private Context context;
    private HorizonAdapt horizonAdapt;


    public VerticalAdapt(Context context, List<Book> booksList /*List<VerticalModel> verticalModelList*/, RecyclerView recyclerView) {
        this.context = context;
        this.booksList = booksList;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.vertical_list_item, viewGroup, false);
        //VerticalAdaptViewHodler verticalAdaptViewHodler = new VerticalAdaptViewHodler(view);
        viewHolder = new VerticalAdaptViewHodler(view);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof VerticalAdaptViewHodler) {

            Book book = booksList.get(i);
            VerticalAdaptViewHodler vh =((VerticalAdaptViewHodler) holder);
            if (book.getData().size() > 0) {
                vh.cardView.setVisibility(View.VISIBLE);
                List<Datum> datum = book.getData();
                vh.tv_generName.setText(book.getCategory());
                vh.tv_seeAll.setText("See All");
                vh.horizonRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                horizonAdapt = new HorizonAdapt(context, datum);
                vh.horizonRecycler.setHasFixedSize(true);
                vh.horizonRecycler.setItemViewCacheSize(10);
                //vh.horizonRecycler.setNestedScrollingEnabled(false);
                vh.horizonRecycler.setAdapter(horizonAdapt);
            }

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return booksList.size();
    }

    public class VerticalAdaptViewHodler extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_generName, tv_seeAll;
        private RecyclerView horizonRecycler;
        private CardView cardView;
        // private VerticalModel vertical_model;

        public VerticalAdaptViewHodler(@NonNull View itemView) {
            super(itemView);

            // context = itemView.getContext();
            tv_generName = itemView.findViewById(R.id.tv_genere_title);
            tv_seeAll = itemView.findViewById(R.id.tv_seeall);
            horizonRecycler = itemView.findViewById(R.id.item_horizontal_list);
            cardView = itemView.findViewById(R.id.cardview_verticalListItem);
            tv_seeAll.setTag(R.integer.btn_seeall_view);
            tv_seeAll.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_seeall:
                    Intent intent = new Intent(context, SeeAllBooksActivity.class);
                    intent.putExtra(Utils.SEE_ALL_KEY, booksList.get(getAdapterPosition()).getCategory());
                    context.startActivity(intent);

                    break;
            }

        }
    }



    public void addBooks(List<Book> booksList) {
        Log.v("addBooks", "isLoading=true");
        for (Book book : booksList)

        {
            Log.v("verticalAdapt", "book:booksList");

            this.booksList.add(book);
        }
        notifyDataSetChanged();
    }

}

