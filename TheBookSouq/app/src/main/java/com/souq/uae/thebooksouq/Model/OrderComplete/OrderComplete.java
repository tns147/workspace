package com.souq.uae.thebooksouq.Model.OrderComplete;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderComplete {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("cart")
    @Expose
    private String cart;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

}