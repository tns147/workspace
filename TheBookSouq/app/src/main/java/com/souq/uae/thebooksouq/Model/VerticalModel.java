package com.souq.uae.thebooksouq.Model;

import java.util.List;

public class VerticalModel {
    private List<HorizonModel> horizonModelLists;
    private String genereName ;
    private String seeAll;

    public VerticalModel(List<HorizonModel> horizonModels,String genereName,String seeAll)
    {
        this.horizonModelLists = horizonModels;
        this.genereName = genereName;
        this.seeAll = seeAll;
    }
    public List<HorizonModel> getHorizonModelLists() {
        return horizonModelLists;
    }

    public String getGenereName() {
        return genereName;
    }

    public String getSeeAll() {
        return seeAll;
    }
}
