package com.souq.uae.thebooksouq.Activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.souq.uae.thebooksouq.R;

public class RegisteredActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Button btn_getStarted;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered);

        findViews();
        performActions();
        btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisteredActivity.this,HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private void findViews()
    {
        toolbar = findViewById(R.id.toolbar_activity_registered);
        btn_getStarted = findViewById(R.id.registered_btn_getStarted);
    }

    private void performActions()
    {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {

    }
    /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itm = item.getItemId();

        switch (itm) {
            case android.R.id.home:
                super.onBackPressed();
                break;}
        return super.onOptionsItemSelected(item);
    }*/
}
