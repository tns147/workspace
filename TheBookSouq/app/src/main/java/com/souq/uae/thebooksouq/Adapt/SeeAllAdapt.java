package com.souq.uae.thebooksouq.Adapt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.souq.uae.thebooksouq.Activity.BookDescriptionActivity;


import com.souq.uae.thebooksouq.Model.SeeAll.Datum;
import com.souq.uae.thebooksouq.R;
import com.souq.uae.thebooksouq.Utility.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SeeAllAdapt extends RecyclerView.Adapter {
    private List<Datum> datumList;
    private Context context;
    private String TAG = SeeAllAdapt.class.getSimpleName();

    public SeeAllAdapt(Context context, List<Datum> datumList) {
        this.context = context;
        this.datumList = datumList;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder;
        Log.d(TAG, "i==view_item");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.seeall_item_layout, viewGroup, false);
        viewHolder = new SeeAllAdaptViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof SeeAllAdaptViewHolder) {
            Log.d(TAG, "OnBindViewHolder: holder instanceof SeeAllAdaptViewHolder");
            //AllBooksModel allBooksModel = allBooksModelList.get(i);
            Datum datum = datumList.get(i);


            ((SeeAllAdaptViewHolder) holder).tv_bookName.setText(datum.getTitle());
            ((SeeAllAdaptViewHolder) holder).tv_authorName.setText("by"+" "+datum.getAuthor());
                if (datum.getDiscount_price()>0) {
                    ((SeeAllAdaptViewHolder) holder).tv_discounted_price.setText(String.valueOf(datum.getDiscount_price())+" "+"AED");
                    ((SeeAllAdaptViewHolder) holder).tv_discounted_price.setTextColor(context.getResources().getColor(R.color.black));
                    ((SeeAllAdaptViewHolder) holder).tv_price.setText(String.valueOf(datum.getPrice()) + " " + "AED");
                    ((SeeAllAdaptViewHolder) holder).tv_price.setPaintFlags(((SeeAllAdaptViewHolder) holder).tv_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
                else {
                    ((SeeAllAdaptViewHolder) holder).tv_discounted_price.setText(String.valueOf(datum.getPrice())+ " " + "AED");
                    ((SeeAllAdaptViewHolder) holder).tv_discounted_price.setTextColor(context.getResources().getColor(R.color.black));
                    ((SeeAllAdaptViewHolder) holder).tv_price.setVisibility(View.INVISIBLE);

                }


                ((SeeAllAdaptViewHolder) holder).tv_merchantName.setText(datum.getMerchant());
            Picasso.get().load(Utils.HOME_BASE_URL_IMAGE+datum.getImageUrl()).into( ((SeeAllAdaptViewHolder) holder).img_bookCover);

        }

    }

    @Override
    public int getItemCount() {

        Log.d(TAG, "getitemCount()");
        return datumList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class SeeAllAdaptViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_bookCover, img_cart;
        TextView tv_bookName, tv_authorName, tv_merchantName, tv_price ,tv_discounted_price;

        public SeeAllAdaptViewHolder(@NonNull View itemView) {
            super(itemView);
            Log.d(TAG, "SeeAllAdaptViewHolder");
            img_bookCover = itemView.findViewById(R.id.imgv_seeallitem_bookCover);
            img_cart = itemView.findViewById(R.id.imgv_seeallitem_cart);
            tv_bookName = itemView.findViewById(R.id.tv_seeallitem_bookName);
            tv_authorName = itemView.findViewById(R.id.tv_seeallitem_authorName);
            tv_merchantName = itemView.findViewById(R.id.tv_seeallitem_merchnat);
            tv_price = itemView.findViewById(R.id.tv_seeallitem_price);
            tv_discounted_price = itemView.findViewById(R.id.tv_seeallitem_discounted_price);
            img_bookCover.setTag(R.integer.btn_seeall_view);
            img_cart.setTag(R.integer.btn_seeall_cart_view);

            img_bookCover.setOnClickListener(this);
            img_cart.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imgv_seeallitem_bookCover:
                    Log.d(TAG, "imgv_seeallitem_bookCover");
                   // Toast.makeText(context, "bookcover", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, BookDescriptionActivity.class);
                    intent.putExtra(Utils.BOOK_ID,String.valueOf(datumList.get(getAdapterPosition()).getId()));
                    context.startActivity(intent);


                   /* BookDescriptionFragment bookDescriptionFragment = new BookDescriptionFragment();
                    HomeActivity homeActivity = (HomeActivity) context;
                    FragmentTransaction fragmentTransaction = homeActivity.getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_home, bookDescriptionFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();*/

                    break;
                case R.id.imgv_seeallitem_cart:
                    Log.d(TAG, "imgv_seeallitem_cart");
                    //Toast.makeText(context, "cart", Toast.LENGTH_SHORT).show();
                    int cartCount = Utils.getCartCount(context);
                    Utils.saveCartCount(context, ++cartCount);
                    Activity activity = (Activity) context;
                    //activity.invalidateOptionsMenu();
                    break;
            }
        }
    }
    public void addData (List<Datum> datumList)
    {
        Log.v("addBooks", "isLoading=true");
        for (Datum datum : datumList)

        {
            Log.v("seeAllAdapt", "book:booksList");

            this.datumList.add(datum);
        }
        notifyDataSetChanged();
    }
}
