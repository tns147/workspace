package com.souq.uae.thebooksouq.Model.ReverseGeoModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Location {

    @SerializedName("LocationId")
    @Expose
    private String locationId;
    @SerializedName("LocationType")
    @Expose
    private String locationType;
    @SerializedName("DisplayPosition")
    @Expose
    private DisplayPosition displayPosition;
    @SerializedName("NavigationPosition")
    @Expose
    private List<NavigationPosition> navigationPosition = null;
    @SerializedName("MapView")
    @Expose
    private MapView mapView;
    @SerializedName("Address")
    @Expose
    private Address address;
    @SerializedName("MapReference")
    @Expose
    private MapReference mapReference;

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public DisplayPosition getDisplayPosition() {
        return displayPosition;
    }

    public void setDisplayPosition(DisplayPosition displayPosition) {
        this.displayPosition = displayPosition;
    }

    public List<NavigationPosition> getNavigationPosition() {
        return navigationPosition;
    }

    public void setNavigationPosition(List<NavigationPosition> navigationPosition) {
        this.navigationPosition = navigationPosition;
    }

    public MapView getMapView() {
        return mapView;
    }

    public void setMapView(MapView mapView) {
        this.mapView = mapView;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public MapReference getMapReference() {
        return mapReference;
    }

    public void setMapReference(MapReference mapReference) {
        this.mapReference = mapReference;
    }

}